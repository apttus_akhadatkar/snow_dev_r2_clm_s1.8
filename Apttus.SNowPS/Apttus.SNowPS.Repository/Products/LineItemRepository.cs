﻿/****************************************************************************************
@Name: AccountRepository.cs
@Author: Rahul Der
@CreateDate: 11 Oct 2017
@Description:To get Cart Line Item finaincial fields 
@UsedBy: This will be used by LineItemController.cs

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web.Script.Serialization;
using Apttus.DataAccess.Common.Model;
using Apttus.DataAccess.Common.Enums;

namespace Apttus.SNowPS.Respository
{
    public class LineItemRepository
    {
        /// <summary>
        /// Account Advance Search and Post
        /// </summary>
        /// <returns></returns>
        #region Properties
        #endregion

        #region Methods

        /// <summary>
        /// Get calculated financial field details of cart line items
        /// </summary>
        /// <param name="productConfigId">productConfigurationId</param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public HttpResponseMessage GetLineItemsFinancialFields(string productConfigId, string accessToken)
        {
            try
            {
                var productsJson = string.Empty;
                accessToken = accessToken != null ? accessToken : Utilities.GetAuthToken();
                //Create AQL Query
                var query = new Query(Constants.OBJ_LINEITEM);

                query.AddColumns(Constants.OBJ_LINEITEM_SELECTFIELD.Split(Constants.CHAR_COMMA));

                Join prodJoin = new Join(Constants.OBJ_LINEITEM, Constants.OBJ_PRODUCT, Constants.FIELD_PRODUCTID, Constants.FIELD_ID, JoinType.LEFT, Constants.OBJ_PRODUCT);
                Join prodConfigJoin = new Join(Constants.OBJ_LINEITEM, Constants.OBJ_PRODUCTCONFIGURATION, Constants.FIELD_CONFIGURATIONID, Constants.FIELD_ID, JoinType.LEFT, Constants.OBJ_PRODUCTCONFIGURATION);
                Join quoteJoin = new Join(Constants.OBJ_PRODUCTCONFIGURATION, Constants.OBJ_QUOTE, Constants.FIELD_QUOTEID, Constants.FIELD_ID, JoinType.LEFT, Constants.OBJ_QUOTE);

                query.AddJoin(prodJoin);
                query.AddJoin(prodConfigJoin);
                query.AddJoin(quoteJoin);

                Expression andExp = new Expression(ExpressionOperator.OR);
                andExp.AddCondition(new Condition("ConfigurationId", FilterOperator.Equal, productConfigId));
                query.SetCriteria(andExp);

                var jsonQuery = query.Serialize();

                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.searchType = SearchType.AQL;
                reqConfig.objectName = Constants.OBJ_LINEITEM;


                var response = Utilities.Search(jsonQuery, reqConfig);
                List<LineItemResponse> lstResultViewModel = new List<LineItemResponse>();
                if (response != null && response.IsSuccessStatusCode)
                {
                    var responseString = response.Content.ReadAsStringAsync().Result;
                    var ListLineItemModel = JObject.Parse(responseString).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToObject<List<LineItemsViewModel>>();

                    //Iterate through line items to calculate financial fileds
                    if (ListLineItemModel != null && ListLineItemModel.Count > 0)
                    {
                        for (int i = 0; i < ListLineItemModel.Count; i++)
                        {
                            LineItemResponse objLineItemsViewModel = new LineItemResponse();
                            objLineItemsViewModel.Id = ListLineItemModel[i].Id;
                            //objLineItemsViewModel.Name = ListLineItemModel[i].Name;

                            //List Price With Adjustments
                            objLineItemsViewModel.ext_ListPriceWithAdjustments = ListLineItemModel[i].BasePrice.ConvertToDecimal();

                            if (ListLineItemModel[i].ListPrice != 0)
                            {
                                objLineItemsViewModel.ext_AdjustmentAmount = Convert.ToDecimal((ListLineItemModel[i].ListPrice * ListLineItemModel[i].Quantity * ListLineItemModel[i].Term) - ListLineItemModel[i].NetPrice);
                                //objLineItemsViewModel.ext_AdjustmentDiscountPercent = Convert.ToDecimal(((ListLineItemModel[i].ListPrice * ListLineItemModel[i].Quantity * ListLineItemModel[i].Term) - ListLineItemModel[i].NetPrice) * 100 / (ListLineItemModel[i].ListPrice * ListLineItemModel[i].Quantity * ListLineItemModel[i].Term));
                                objLineItemsViewModel.ext_AdjustmentDiscountPercent = Convert.ToDecimal(objLineItemsViewModel.ext_AdjustmentAmount * 100 / (ListLineItemModel[i].ListPrice * ListLineItemModel[i].Quantity * ListLineItemModel[i].Term));
                            }

                            if (ListLineItemModel[i].crm_Product.Id != null)
                            {
                                if (ListLineItemModel[i].crm_Product.ext_Family != null && ListLineItemModel[i].crm_Product.ext_Family.Value.ToString().ToLower() != Constants.STR_SUBSCRIPTION)
                                {
                                    objLineItemsViewModel.ext_AnnualContractValue = 0;
                                    objLineItemsViewModel.ext_AnnualListPrice = 0;
                                }
                                else
                                {
                                    if (ListLineItemModel[i].SellingFrequency != null && ListLineItemModel[i].SellingFrequency.Value.ToString().ToLower() == Constants.STR_YEARLY)
                                    {
                                        objLineItemsViewModel.ext_AnnualContractValue = Convert.ToDecimal(ListLineItemModel[i].NetUnitPrice * ListLineItemModel[i].Quantity);
                                        objLineItemsViewModel.ext_AnnualListPrice = Convert.ToDecimal(ListLineItemModel[i].ListPrice * ListLineItemModel[i].Quantity);
                                    }
                                    else
                                    {
                                        objLineItemsViewModel.ext_AnnualContractValue = Convert.ToDecimal(ListLineItemModel[i].NetUnitPrice * 12 * ListLineItemModel[i].Quantity);
                                        objLineItemsViewModel.ext_AnnualListPrice = Convert.ToDecimal(ListLineItemModel[i].ListPrice * 12 * ListLineItemModel[i].Quantity);
                                    }
                                }
                            }
                            objLineItemsViewModel.ext_EstimatedTax = ListLineItemModel[i].ext_EstimatedTax.ConvertToDecimal();

                            //Estimated Total: Total Value + EstimatedTax 
                            objLineItemsViewModel.ext_EstimatedTotal = ListLineItemModel[i].NetPrice.ConvertToDecimal() + objLineItemsViewModel.ext_EstimatedTax.ConvertToDecimal();

                            //Sales Price
                            objLineItemsViewModel.ext_SalesPrice = ListLineItemModel[i].NetUnitPrice.ConvertToDecimal();

                            lstResultViewModel.Add(objLineItemsViewModel);
                        }
                    }
                }
                LineItemModel objLineItemModel = new LineItemModel();
                objLineItemModel.ListLineItems = lstResultViewModel;
                //Save updated Line Item details through API..
                var responseResult = UpdateLineItems(lstResultViewModel, accessToken);
                return responseResult;
            }
            catch (Exception ex)
            {
                //TODO
                throw;
            }
        }

        /// <summary>
        /// Update calculated line items details
        /// </summary>
        /// <param name="lineItems">list of fileds to be update</param>
        /// <returns></returns>
        public HttpResponseMessage UpdateLineItems(List<LineItemResponse> lineItems, string accessToken)
        {
            var reqConfig = new RequestConfigModel();
            reqConfig.accessToken = accessToken;
            reqConfig.objectName = Constants.OBJ_CPQ_LINEITEM;
            reqConfig.externalFilterField = Constants.FIELD_LINEITEMID;
            reqConfig.apttusFilterField = Constants.FIELD_ID;
            var responseString = Utilities.Update(lineItems, reqConfig);
            return responseString;
        }


        /// <summary>
        /// Get Currency exchange rate based on currency code
        /// </summary>
        /// <param name="currencyCode"></param>
        /// <returns></returns>
        public DailyExcnageRate GetExchangeRateDetails(string currencyCode, string accessToken)
        {
            //currencyCode = "USD";
            DailyExcnageRate ExchangeRates = new DailyExcnageRate();
            accessToken = accessToken != null ? accessToken : Utilities.GetAuthToken();

            var reqExchangeRateLineItem = Utilities.GetRequestConfiguration(Constants.OBJ_DAILYEXCHANGERATE, Constants.FIELD_CURRENCYCODE, Constants.FIELD_CURRENCYCODE);
            var dictContent = new List<Dictionary<string, object>> { new Dictionary<string, object>() { { "ext_CurrencyCode", currencyCode } } };
            var resExchangeRate = Utilities.Search(dictContent, reqExchangeRateLineItem);

            if (resExchangeRate != null && resExchangeRate.IsSuccessStatusCode)
            {
                var responseString = JObject.Parse(resExchangeRate.Content.ReadAsStringAsync().Result).SelectToken("SerializedResultEntities").ToString();
                List<DailyExcnageRate> ListExchgRates = JsonConvert.DeserializeObject<List<DailyExcnageRate>>(responseString);
                // Getting last Created/Modified currency rate
                ExchangeRates = ListExchgRates.OrderByDescending(x => x.ModifiedOn).FirstOrDefault();
            }
            return ExchangeRates;
        }

        #endregion

    }
}