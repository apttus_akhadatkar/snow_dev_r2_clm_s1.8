﻿/****************************************************************************************
@Name: ProductRepository.cs
@Author: Mahesh Patel
@CreateDate: 1 Sep 2017
@Description: Gets Product Details 
@UsedBy: This will be used by API to get Product Details

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

using Apttus.DataAccess.Common.Enums;
using Apttus.DataAccess.Common.Model;
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Apttus.SNowPS.Respository
{
	public class ProductRepository
	{
		private string _accessToken;

		/// <summary>
		/// Get product details for given product id
		/// </summary>
		/// <param name="productId"></param>
		/// <param name="accessToken"></param>
		/// <returns></returns>
		public string GetProducts(string productId, string accessToken)
		{
			try
			{
				var productsJson = string.Empty;
				_accessToken = accessToken;

				//Create AQL Query
				var query = new Query(Constants.OBJ_PRODUCT);

                Expression joinExpForOption = new Expression();
                joinExpForOption.AddCondition(new Condition("Options.ParentProductId", FilterOperator.Equal, productId));

                query.AddColumns(Constants.OBJ_PRODUCT_SELECTFIELD.Split(Constants.CHAR_COMMA));
				Join prodJoin = new Join(Constants.OBJ_PRODUCT, Constants.OBJ_PRODUCTOPTIONCOMPONENT, Constants.FIELD_ID, Constants.FIELD_COMPONENTPRODUCTID, JoinType.LEFT, Constants.ALIAS_OPTIONS, joinExpForOption);
				Join pluginJoin = new Join(Constants.OBJ_PRODUCT, Constants.OBJ_SNPLUGINS, Constants.FIELD_ID, Constants.FIELD_EXT_LKP, JoinType.LEFT, Constants.ALIAS_PLUGINS);
				Join attrGrpMmbrJoin = new Join(Constants.OBJ_PRODUCT, Constants.OBJ_PRODUCTATTRIBUTEGROUPMEMBER, Constants.FIELD_ID, Constants.FIELD_PRODUCTID, JoinType.LEFT, Constants.OBJ_PRODUCTATTRIBUTEGROUPMEMBER);
				Join attrGrpJoin = new Join(Constants.OBJ_PRODUCTATTRIBUTEGROUPMEMBER, Constants.OBJ_PRODUCTATTRIBUTEGROUP, Constants.FIELD_ATTRIBUTEGROUPID, Constants.FIELD_ID, JoinType.LEFT, Constants.OBJ_PRODUCTATTRIBUTEGROUP);
				Join attrJoin = new Join(Constants.OBJ_PRODUCTATTRIBUTEGROUP, Constants.OBJ_PRODUCTATTRIBUTE, Constants.FIELD_ID, Constants.FIELD_ATTRIBUTEGROUPID, JoinType.LEFT, Constants.ALIAS_ATTRIBUTES);

				query.AddJoin(prodJoin);
				query.AddJoin(pluginJoin);
				query.AddJoin(attrGrpMmbrJoin);
				query.AddJoin(attrGrpJoin);
				query.AddJoin(attrJoin);

				Expression andExp = new Expression(ExpressionOperator.OR);
				andExp.AddCondition(new Condition("Options.ParentProductId", FilterOperator.Equal, productId));
				andExp.AddCondition(new Condition(Constants.FIELD_ID, FilterOperator.Equal, productId, Constants.OBJ_PRODUCT));
				query.SetCriteria(andExp);

				var jsonQuery = query.Serialize();

				var reqConfig = new RequestConfigModel();
				reqConfig.accessToken = accessToken;
				reqConfig.searchType = SearchType.AQL;
				reqConfig.objectName = Constants.OBJ_PRODUCT;

				//Get the product details
				var response = Utilities.Search(jsonQuery, reqConfig);

				if (response != null && response.IsSuccessStatusCode)
				{
					var responseString = response.Content.ReadAsStringAsync().Result;
					var products = JObject.Parse(responseString).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToObject<List<Product>>();

					//Get json request for Mule
					if (products != null && products.Count > 0)
						productsJson = CreateProductHierarchy(products);
				}

				return productsJson;
			}
			catch
			{
				//TODO
				throw;
			}
		}

		/// <summary>
		/// Create ProductHierarchy and json for Mule API
		/// </summary>
		/// <param name="products"></param>
		/// <returns></returns>
		private string CreateProductHierarchy(List<Product> products)
		{
			//Get Parent Product
			var parentProdHierarchy = products.Where(data => data.Options.ParentProductId == null).
				Select(data => new ProductsHierarchy
				{
					Id = data.Id,
					Name = data.Name,
					Description = data.Description,
					ProductCode = data.ProductCode,
					ConfigurationType = data.ConfigurationType != null ? data.ConfigurationType.Key : null,
					Family = data.Family != null ? data.Family.Key : null,
					EndDate = data.ExpirationDate,
					StartDate = data.EffectiveDate,
					Uom = data.Uom != null ? data.Uom.Key: null,
					AppVersion = data.AppVersion,
					BusinessUnit = data.BusinessUnit != null ? data.BusinessUnit.Key : null,
					CalculateACV = data.CalculateACV,
					ContributesTo = data.ContributesTo != null ? data.ContributesTo.Key : null,
					DefaultRevRec = data.DefaultRevRec != null ? data.DefaultRevRec.Key : null,
					ExcludeFromOpportunityFinancials = data.ExcludeFromOpportunityFinancials,
					ExcludeFromQuoteFinancials = data.ExcludeFromQuoteFinancials,
					ExtFamily = data.ExtFamily != null ? data.ExtFamily.Key : null,
					Family1 = data.Family1 != null ? data.Family1.Key : null,
					Family2 = data.Family2 != null ? data.Family2.Key : null,
					Family3 = data.Family3 != null ? data.Family3.Key : null,
					FilteredOrderTypes = data.FilteredOrderTypes !=null && data.FilteredOrderTypes.Length > 0 ? data.FilteredOrderTypes[0]:null,
					HasQuantityRestrictions = data.HasQuantityRestrictions,
					LicenseProductType = data.LicenseProductType != null ? data.LicenseProductType.Key : null,
					MaximumQuantity = data.MaximumQuantity,
					MinimumQuantity = data.MinimumQuantity,
					ProductApprovalStatus = data.ProductApprovalStatus != null ? data.ProductApprovalStatus.Key : null,
					ProductFirstReleaseVersion = data.ProductFirstReleaseVersion != null ? data.ProductFirstReleaseVersion.Key : null,
					ProductLastReleaseVersion = data.ProductLastReleaseVersion != null ? data.ProductLastReleaseVersion.Key : null,
					ProductOptionApplicationPercentageAllocation = data.ProductOptionApplicationPercentageAllocation,
					ProductLifecycleStatus = data.ProductLifecycleStatus != null ? data.ProductLifecycleStatus.Key : null,
					RevRecGroup = data.RevRecGroup != null ? data.RevRecGroup.Key : null,
					SKU = data.SKU,
					SysId = data.SysId,
					TaxRateType = data.TaxRateType != null ? data.TaxRateType.Key : null,
					TechnologyPartnerEmail = data.TechnologyPartnerEmail,
					TechnologyPartnerName = data.TechnologyPartnerName,
					UAAttribute = data.UAAttribute != null ? data.UAAttribute.Key : null,
					OrderFormSection = data.OrderFormOnly != null ? data.OrderFormOnly.Key : null,
					NeedDeploymentApplication = data.NeedDeploymentApplication,
					EnableHIRSA = data.EnableHIRSA,
					RequiresSurvey = data.RequiresSurvey,
					DeploymentCategory = data.DeploymentCategory != null ? data.DeploymentCategory.Key : null,
					RequiresGoLive = data.RequiresGoLive,
					Practice = data.Practice != null ? data.Practice.Key : null,
					RoleGroup = data.RoleGroup != null ? data.RoleGroup.Key : null,
					RequiresStatusReport = data.RequiresStatusReport,
					Applications = new List<Application>(),
					Attributes = data.Attributes != null && data.Attributes.Field != null ? new List<Attributes>() { data.Attributes } : null
				}).ToList();

			//Add Attributes to Product
			var parentProduct = new ProductsHierarchy();
			foreach (ProductsHierarchy prodHierarchy in parentProdHierarchy)
			{
				if (parentProduct.Id != prodHierarchy.Id)
					parentProduct = prodHierarchy;
				else if (prodHierarchy.Attributes != null)
					parentProduct.Attributes.AddRange(prodHierarchy.Attributes);
			}

			//Add applications with plugins
			foreach (var product in products)
			{
				if (product.Options.ParentProductId != null)
				{
					if (parentProduct != null && parentProduct.Applications != null)
					{
						if (!parentProduct.Applications.Exists(data => data.Id == product.Id))
						{
							var app = new Application()
							{
								Id = product.Id,
								ParentID = product.Options.ParentProductId.Id,
								Name = product.Name,
								SKUNumber = product.SKU,
								ConfigurationType = product.ConfigurationType != null ? product.ConfigurationType.Key : null,
								ProductCode = product.ProductCode,
								BusinessUnit = product.BusinessUnit != null ? product.BusinessUnit.Key : null,
								Family1 = product.Family1 != null ? product.Family1.Key : null,
								Family2 = product.Family2 != null ? product.Family2.Key : null,
								Family3 = product.Family3 != null ? product.Family3.Key : null,
								FirstReleaseVersion = product.ProductFirstReleaseVersion != null ? product.ProductFirstReleaseVersion.Key : null,
								LastReleaseVersion = product.ProductLastReleaseVersion != null ? product.ProductLastReleaseVersion.Key : null,
								Description = product.Description
							};
							if (product.Plugins != null && !string.IsNullOrEmpty(product.Plugins.ext_PluginId))
							{
								app.Plugins = new List<Plugins>();
								app.Plugins.Add(product.Plugins);
							}
							parentProduct.Applications.Add(app);
						}
						else
						{
							if (parentProduct.Applications.Where(data => data.Id == product.Id).FirstOrDefault().Plugins != null && product.Plugins != null && !string.IsNullOrEmpty(product.Plugins.ext_PluginId))
								parentProduct.Applications.Where(data => data.Id == product.Id).FirstOrDefault().Plugins.Add(product.Plugins);
							else
							{
								if (product.Plugins != null && !string.IsNullOrEmpty(product.Plugins.ext_PluginId))
									parentProduct.Applications.Where(data => data.Id == product.Id).FirstOrDefault().Plugins = new List<Plugins>() { product.Plugins };
							}
						}
					}

				}
				else   // Option Product 
				{
					if (!parentProduct.Applications.Exists(data => data.Id == product.Id) && product.ConfigurationType.Key.Equals("Option"))
					{
						var app = new Application()
						{
							Id = product.Id,
							ParentID = null,
							Name = product.Name,
							SKUNumber = product.SKU,
							ConfigurationType = product.ConfigurationType != null ? product.ConfigurationType.Key : null,
							ProductCode = product.ProductCode,
							BusinessUnit = product.BusinessUnit != null ? product.BusinessUnit.Key : null,
							Family1 = product.Family1 != null ? product.Family1.Key : null,
							Family2 = product.Family2 != null ? product.Family2.Key : null,
							Family3 = product.Family3 != null ? product.Family3.Key : null,
							FirstReleaseVersion = product.ProductFirstReleaseVersion != null ? product.ProductFirstReleaseVersion.Key : null,
							LastReleaseVersion = product.ProductLastReleaseVersion != null ? product.ProductLastReleaseVersion.Key : null,
							Description = product.Description
						};
						if (product.Plugins != null && !string.IsNullOrEmpty(product.Plugins.ext_PluginId))
						{
							app.Plugins = new List<Plugins>();
							app.Plugins.Add(product.Plugins);
						}
						parentProduct.Applications.Add(app);
					}
				}
			}

			//Get attribute values based on rules specified. If no rules, get all values
			if (parentProduct.Attributes != null && parentProduct.Attributes.Count > 0)
			{
				GetAttributes(parentProduct.Id, parentProduct.Attributes);
				parentProduct.ProductAttributes = parentProduct.Attributes.Select(data => new ProductAttributes { Name = data.Name, Values = data.Values }).ToList();
			}

			//Applications
			if (parentProduct.Applications != null && parentProduct.Applications.Count == 0)
				parentProduct.Applications = null;

			//Get json
			var productJson = JsonConvert.SerializeObject(parentProduct);

			return productJson;
		}

		/// <summary>
		/// Get product attribute values based on rules specified. If no rules, get all values
		/// </summary>
		/// <param name="productId"></param>
		/// <param name="accessToken"></param>
		/// <returns></returns>
		private void GetAttributes(string productId, List<Attributes> attributes)
		{
			var productsJson = string.Empty;

			//Get All Attribute Values
			var reqAttrConfig = new RequestConfigModel();
			reqAttrConfig.accessToken = _accessToken;
			reqAttrConfig.objectName = Constants.OBJ_PRODUCTATTRIBUTEVALUE;

			var response = Utilities.GetMetadata(reqAttrConfig);

			if (response != null && response.IsSuccessStatusCode)
			{
				var responseString = response.Content.ReadAsStringAsync().Result;

				//Fetch and assign attribute values
				foreach (Attributes attribute in attributes)
				{
					if (attribute != null && attribute.Field != null)
					{
						var fieldsObj = JObject.Parse(responseString).SelectToken(Constants.NODE_FIELDS).SelectToken(attribute.Field);

						if (fieldsObj != null && fieldsObj.Type != JTokenType.Null)
						{
							var optionSetObj = fieldsObj.SelectToken(Constants.NODE_SELECTOPTIONSET);
							if (optionSetObj != null && optionSetObj.Type != JTokenType.Null)
							{
								var ruleActions = optionSetObj.SelectToken(Constants.NODE_SELECTOPTIONS).ToObject<List<Dictionary<string, object>>>();
								var values = ruleActions.Select(data => Convert.ToString(data[Constants.NODE_OPTIONVALUE])).ToList();
								attribute.Name = Convert.ToString(fieldsObj[Constants.FIELD_DISPLAYNAME]);
								attribute.Values = new List<string>();
								attribute.Values.AddRange(values);
							}
						}
					}
				}

				//Get Rules
				var attrRuleRequest = new AttributeRuleRequestModel();
				attrRuleRequest.requestModel = new AttributeRuleRequest();
				attrRuleRequest.requestModel.ProductId = productId;
				attrRuleRequest.requestModel.NumberOfRecordsPerChunk = 500;

				var jsonRuleRequest = JsonConvert.SerializeObject(attrRuleRequest);

				var appUrl = String.Format(Convert.ToString(ConfigurationManager.AppSettings[Constants.CONFIG_ADMINAPPURL]), productId);

				var reqConfig = new RequestConfigModel();
				reqConfig.accessToken = _accessToken;
				reqConfig.searchType = SearchType.CpqAdminAPI;
				reqConfig.appUrl = appUrl;

				response = Utilities.Search(jsonRuleRequest, reqConfig);

				if (response != null && response.IsSuccessStatusCode)
				{
					var responseRuleObj = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_RESULT).SelectToken(Constants.NODE_GENERICRULEDOS).ToObject<List<Dictionary<string, object>>>();

					//Get actions for the rules
					foreach (Dictionary<string, object> rule in responseRuleObj)
					{
						//Create AQL Query
						var query = new Query(Constants.OBJ_PRODUCTATTRIBUTERULEACTION);

						query.AddColumns(Constants.FIELD_VALUEEXPRESSION, Constants.FIELD_ACTION, Constants.FIELD_FIELD);
						var queryExpression = new Expression(ExpressionOperator.OR);
						queryExpression.AddCondition(new Condition(Constants.FIELD_PRODUCTATTRIBUTERULEID, FilterOperator.Equal, rule[Constants.FIELD_ID]));
						query.SetCriteria(queryExpression);

						var jsonQuery = query.Serialize();

						//Get Rule Actions
						reqConfig = new RequestConfigModel();
						reqConfig.accessToken = _accessToken;
						reqConfig.searchType = SearchType.AQL;
						reqConfig.objectName = Constants.OBJ_PRODUCTATTRIBUTERULEACTION;

						response = Utilities.Search(jsonQuery, reqConfig);

						if (response != null && response.IsSuccessStatusCode)
						{
							var responseActionsString = response.Content.ReadAsStringAsync().Result;
							var responseActionsObj = JObject.Parse(responseActionsString).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToObject<List<Dictionary<string, object>>>();

							foreach (Dictionary<string, object> responseAction in responseActionsObj)
							{
								var field = responseAction.ContainsKey(Constants.FIELD_FIELD) ? Convert.ToString(responseAction[Constants.FIELD_FIELD]) : null;
								var action = responseAction.ContainsKey(Constants.FIELD_ACTION) ? JsonConvert.DeserializeObject<Dictionary<string, string>>(Convert.ToString(responseAction[Constants.FIELD_ACTION])).Values.First() : null;
								var valueExpression = responseAction.ContainsKey(Constants.FIELD_VALUEEXPRESSION) ? Convert.ToString(responseAction[Constants.FIELD_VALUEEXPRESSION]).Trim(Constants.CHAR_SINGLEQUOTE) : null;

								if (field != null && action != null && valueExpression != null)
								{
									field = field.Substring(field.LastIndexOf(Constants.CHAR_DOT) + 1);
									var values = valueExpression.Split(Constants.CHAR_SEMICOLON);

									if (attributes.Exists(data => data.Field == field))
									{
										foreach (var attribute in attributes.Where(data => data.Field == field))
										{
											switch (action)
											{
												case Constants.ACTIONTYPE_ALLOW:
													attribute.Values = values.ToList();
													break;
												case Constants.ACTIONTYPE_HIDDEN:
												case Constants.ACTIONTYPE_DISABLED:
													foreach (string value in values)
													{
														attribute.Values.Remove(value);
													}
													break;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
