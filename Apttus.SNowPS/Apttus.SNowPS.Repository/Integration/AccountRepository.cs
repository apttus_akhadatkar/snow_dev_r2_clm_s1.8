﻿/****************************************************************************************
@Name: AccountRepository.cs
@Author: Varun Shah
@CreateDate: 7 Oct 2017
@Description: Account Related Computations
@UsedBy: This will be used by ContactController.cs

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;

namespace Apttus.SNowPS.Respository
{
    public class AccountRepository
    {
        #region Properties

        private string muleXClientId = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXCLIENTID];
        private string muleXSourceSystem = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXSOURCESYSTEM];

        #endregion

        #region Methods

        /// <summary>
        /// Resolves external ids and upserts accounts using standard API
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        public HttpResponseMessage UpsertAccount(List<Dictionary<string, object>> dictContent, string accessToken)
        {
            try
            {
                var errors = new List<ErrorInfo>();
                var responseAccount = new HttpResponseMessage();

                //Convert content to case-insensitive
                dictContent = Utilities.GetCaseIgnoreDictContent(dictContent);

                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.select = new string[] { Constants.FIELD_NAME, Constants.FIELD_EXTERNALID };

                //Resolve Parent MDM Account Id with Apttus Account Id
                reqConfig.objectName = Constants.OBJ_ACCOUNT;
                reqConfig.externalFilterField = Constants.FIELD_PARENTID;
                reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                reqConfig.resolvedID = Constants.FIELD_PARENTID;

                ////Resolve Field Territory with Apttus Territory Id
                reqConfig.objectName = Constants.OBJ_TERRITORY;
                reqConfig.externalFilterField = Constants.FIELD_ACCOUNTFIELDTERRITOTYMDMID;
                reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                reqConfig.resolvedID = Constants.FIELD_ACCOUNTFIELDTERRITORYID;
                Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                ////Resolve PS Territory with Apttus Territory Id
                reqConfig.objectName = Constants.OBJ_TERRITORY;
                reqConfig.externalFilterField = Constants.FIELD_ACCOUNTPSTERRITOTYMDMID;
                reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                reqConfig.resolvedID = Constants.FIELD_ACCOUNTPSTERRITORYID;
                Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                //Remove unresolved requests
                Utilities.RemoveUnresolvedRequests(dictContent, errors, Constants.FIELD_EXTERNALID);

                if (dictContent != null && dictContent.Count > 0)
                {
                    //Upsert Account
                    reqConfig.objectName = Constants.OBJ_ACCOUNT;
                    reqConfig.UpsertKey = Constants.FIELD_EXTERNALID;
                    responseAccount = Utilities.Upsert(dictContent, reqConfig);

                    //Create Response by adding custom errors
                    responseAccount = Utilities.CreateResponse(responseAccount, errors);
                }
                else
                {
                    var bulkResponseModel = new BulkOperationResponseModel();
                    var jsonResponse = JsonConvert.SerializeObject(bulkResponseModel);
                    responseAccount.Content = Utilities.CreateJsonContent(jsonResponse);

                    //Create Response by adding custom errors
                    responseAccount = Utilities.CreateResponse(responseAccount, errors);
                }

                //TODO: Create Legal Address

                return responseAccount;
            }
            catch (Exception)
            {
                //TODO
                throw;
            }
        }

        /// <summary>
        /// Get legal address data from request
        /// </summary>
        /// <param name="dictContent"></param>
        /// <param name="addressContent"></param>
        public void GetAddressData(List<Dictionary<string, object>> dictContent, List<Dictionary<string, object>> addressContent, BulkOperationResponseModel responseAccObj)
        {
            try
            {
                //Get Inserted and Updated Accounts
                var resInsertedAccounts = responseAccObj.Inserted.ToList();
                var resUpdatedAccounts = responseAccObj.Updated.ToList();

                var insertedJson = JsonConvert.SerializeObject(resInsertedAccounts);
                var updatedJson = JsonConvert.SerializeObject(resUpdatedAccounts);

                var dictInserted = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(insertedJson);
                var dictUpdated = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(updatedJson);

                //Get the Addresses for Inserted and Updated Accounts
                foreach (Dictionary<string, object> account in dictContent)
                {
                    var insertedAccountId = dictInserted.Where(data => data.ContainsKey(Constants.FIELD_ID) && data.ContainsKey(Constants.FIELD_EXTERNALID) && account.ContainsKey(Constants.FIELD_EXTERNALID)
                                                   && Convert.ToString(data[Constants.FIELD_EXTERNALID]) == Convert.ToString(account[Constants.FIELD_EXTERNALID])).Select(data => data[Constants.FIELD_ID]).FirstOrDefault();

                    var updatedAccountId = dictUpdated.Where(data => data.ContainsKey(Constants.FIELD_ID) && data.ContainsKey(Constants.FIELD_EXTERNALID) && account.ContainsKey(Constants.FIELD_EXTERNALID)
                                                   && Convert.ToString(data[Constants.FIELD_EXTERNALID]) == Convert.ToString(account[Constants.FIELD_EXTERNALID])).Select(data => data[Constants.FIELD_ID]).FirstOrDefault();

                    if ((insertedAccountId != null || updatedAccountId != null) && (!string.IsNullOrEmpty(Convert.ToString(insertedAccountId)) || !string.IsNullOrEmpty(Convert.ToString(updatedAccountId))))
                    {
                        var address = new Dictionary<string, object>();
                        var accountId = insertedAccountId != null ? insertedAccountId : updatedAccountId;

                        if (account.ContainsKey(Constants.FIELD_NAME))
                            address.Add(Constants.FIELD_NAME, account[Constants.FIELD_NAME]);

                        if (account.ContainsKey(Constants.FIELD_EXT_ADDRESSLINE1))
                            address.Add(Constants.FIELD_EXT_ADDRESSLINE1, account[Constants.FIELD_EXT_ADDRESSLINE1]);

                        if (account.ContainsKey(Constants.FIELD_EXT_ADDRESSLINE2))
                            address.Add(Constants.FIELD_EXT_ADDRESSLINE2, account[Constants.FIELD_EXT_ADDRESSLINE2]);

                        if (account.ContainsKey(Constants.FIELD_EXT_CITY))
                            address.Add(Constants.FIELD_CITY, account[Constants.FIELD_EXT_CITY]);

                        if (account.ContainsKey(Constants.FIELD_EXT_STATE))
                            address.Add(Constants.FIELD_STATE, account[Constants.FIELD_EXT_STATE]);

                        if (account.ContainsKey(Constants.FIELD_EXT_COUNTRY))
                            address.Add(Constants.FIELD_COUNTRY, account[Constants.FIELD_EXT_COUNTRY]);

                        if (account.ContainsKey(Constants.FIELD_EXT_POSTALCODE))
                            address.Add(Constants.FIELD_POSTALCODE, account[Constants.FIELD_EXT_POSTALCODE]);

                        //IsPrimary True, Primary Bill To True and Primary Ship To True
                        if (address.Count > 0)
                        {
                            if (insertedAccountId != null)
                            {
                                address.Add(Constants.FIELD_EXT_ISPRIMARY, true);
                                address.Add(Constants.FIELD_EXT_PRIMARYBILLTO, true);
                                address.Add(Constants.FIELD_EXT_PRIMARYSHIPTO, true);
                                address.Add(Constants.FIELD_EXT_ISBILLTO, true);
                                address.Add(Constants.FIELD_EXT_ISSHIPTO, true);
                            }

                            address.Add(Constants.FIELD_EXTERNALID, account[Constants.FIELD_EXTERNALID] + "-LegalAddress");
                            address.Add(Constants.FIELD_ACCOUNTID, accountId);

                            addressContent.Add(address);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get Primary Address Id
        /// </summary>
        /// <param name="addressContent"></param>
        /// <param name="responseAddrObj"></param>
        public void GetPrimaryAddresses(List<Dictionary<string, object>> addressContent, List<Dictionary<string, object>> primaryAddressContent, BulkOperationResponseModel responseAddrObj)
        {
            try
            {
                //Get Inserted and Updated Accounts
                var resInsertedAddr = responseAddrObj.Inserted.ToList();
                var resUpdatedAddr = responseAddrObj.Updated.ToList();

                var insertedJson = JsonConvert.SerializeObject(resInsertedAddr);
                var updatedJson = JsonConvert.SerializeObject(resUpdatedAddr);

                var dictInserted = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(insertedJson);
                var dictUpdated = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(updatedJson);

                if (dictUpdated != null)
                {
                    dictUpdated.AddRange(dictInserted);
                }

                //Get the Addresses for Inserted and Updated Accounts
                foreach (Dictionary<string, object> address in addressContent)
                {
                    var primaryAddress = dictUpdated.Where(data => data.ContainsKey(Constants.FIELD_ID) && data.ContainsKey(Constants.FIELD_EXTERNALID) && address.ContainsKey(Constants.FIELD_EXTERNALID) && address.ContainsKey(Constants.FIELD_ACCOUNTID)
                                                   && Convert.ToString(data[Constants.FIELD_EXTERNALID]) == Convert.ToString(address[Constants.FIELD_EXTERNALID])).
                                                   Select(data => new Dictionary<string, object> { { Constants.FIELD_EXT_PRIMARYADDRESSID, data[Constants.FIELD_ID] }, { Constants.FIELD_ID, address[Constants.FIELD_ACCOUNTID] } }).FirstOrDefault();

                    if (primaryAddress != null && primaryAddress.Count > 0)
                    {
                        primaryAddressContent.Add(primaryAddress);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update Primary Address
        /// </summary>
        /// <param name="dictContent"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public HttpResponseMessage UpdatePrimaryAddressId(List<Dictionary<string, object>> dictContent, string accessToken)
        {
            try
            {
                //Convert content to case-insensitive
                dictContent = Utilities.GetCaseIgnoreDictContent(dictContent);

                //Upsert Account
                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.objectName = Constants.OBJ_ACCOUNT;
                reqConfig.UpsertKey = Constants.FIELD_ID;
                var responseAccount = Utilities.Upsert(dictContent, reqConfig);

                return responseAccount;
            }
            catch (Exception)
            {
                //TODO
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dictContent"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public HttpResponseMessage UpsertPrimaryAddress(List<Dictionary<string, object>> dictContent, string accessToken)
        {
            try
            {
                //Convert content to case-insensitive
                dictContent = Utilities.GetCaseIgnoreDictContent(dictContent);

                //Upsert Account
                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.objectName = Constants.OBJ_ACCOUNTLOCATION;
                reqConfig.UpsertKey = Constants.FIELD_EXTERNALID;
                var responseAccount = Utilities.Upsert(dictContent, reqConfig);

                return responseAccount;
            }
            catch (Exception)
            {
                //TODO
                throw;
            }
        }

        /// <summary>
        /// Use for Account Sync
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        public HttpResponseMessage SendAccountDataToMule(List<Dictionary<string, object>> dictContent)
        {
            try
            {
                string objResponse = string.Empty;
                string resAccountString = string.Empty;
                string accountUrl = ConfigurationManager.AppSettings[Constants.CONFIG_MOCKAPIURL];
                string accountAPIName = ConfigurationManager.AppSettings[Constants.CONFIG_ACCOUNTSYNCAPINAME];
                string accountXBatchID = ConfigurationManager.AppSettings[Constants.CONFIG_ACCOUNTXBATCHID];

                //GetApttusAccountData
                dictContent = Utilities.GetCaseIgnoreDictContent(dictContent);
                var dictAcc = dictContent.FirstOrDefault().ToDictionary(k => k.Key, k => Convert.ToString(k.Value));
                string eventType = string.Empty;

                if (dictAcc.ContainsKey(Constants.FIELD_EVENTTYPE))
                {
                    eventType = dictAcc[Constants.FIELD_EVENTTYPE];
                    dictContent.FirstOrDefault().Remove(Constants.FIELD_EVENTTYPE);
                }

                var requestConfig = Utilities.GetRequestConfiguration(Constants.OBJ_ACCOUNT, Constants.FIELD_ID, Constants.FIELD_ID);

                var resMessageAccount = Utilities.Search(dictContent, requestConfig);

                if (resMessageAccount.IsSuccessStatusCode)
                {
                    resAccountString = resMessageAccount.Content.ReadAsStringAsync().Result;
                    var objresponseAccount = JObject.Parse(resAccountString).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToObject<List<Dictionary<string, object>>>().FirstOrDefault();
                    object dt;
                    objresponseAccount.TryGetValue("ModifiedOn", out dt);
                    string ModifiedOn = dt != null ? Convert.ToDateTime(dt).ToString("yyyy-MM-dd'T'HH:mm:ss.fff") : null;
                    var dictAccounts = new Dictionary<string, string>();
                    if (objresponseAccount != null && objresponseAccount.Count > 0)
                    {
                        //Create Account Payload
                        dictAccounts = objresponseAccount.ToDictionary(k => k.Key, k => Convert.ToString(k.Value));
                        dictAccounts[Constants.FIELD_TYPE] = !string.IsNullOrEmpty(Convert.ToString(dictAccounts[Constants.FIELD_TYPE])) ? JsonConvert.DeserializeObject<KeyValuePair<string, string>>(dictAccounts[Constants.FIELD_TYPE]).Key : Constants.STR_NONE;
                        string strAccounts = new JavaScriptSerializer().Serialize(dictAccounts);

                        var responseAccountModel = JsonConvert.DeserializeObject<AccountModel>(strAccounts);
                        responseAccountModel.accountId = responseAccountModel.srcPartyId;
                        responseAccountModel.accountName = responseAccountModel.partyNm;
                        responseAccountModel.boClassCode = Constants.CUSTOM_SYNC_BOCLASSCODE;
                        responseAccountModel.partyTypCd = responseAccountModel.partyTypCd;
                        responseAccountModel.sourceSystem = Constants.CUSTOM_SYNC_SOURCESYSTEM;
                        responseAccountModel.lastUpdatedDateTime = ModifiedOn;
                        if (eventType == Constants.FIELD_UPDATECUSTOMEREVENTTYPE)
                            responseAccountModel.eventType = eventType;
                        else
                        {
                            responseAccountModel.eventType = Constants.FIELD_ACCOUNTUPDATEEVENTTYPE;
                            responseAccountModel.partyTypCd = responseAccountModel.partyTypCd;
                        }

                        //Add Primary address for account in mule post request
                        if (dictAccounts.ContainsKey(Constants.FIELD_EXT_PRIMARYADDRESSID))
                        {
                            var primaryAddId = dictAccounts[Constants.FIELD_EXT_PRIMARYADDRESSID];
                            var objPrimaryAddId = JsonConvert.DeserializeObject<LookUp>(primaryAddId);

                            var reqConfigAccountLocation = new RequestConfigModel();
                            reqConfigAccountLocation.accessToken = Utilities.GetAuthToken();
                            reqConfigAccountLocation.apttusFilterField = Constants.FIELD_ID;
                            reqConfigAccountLocation.externalFilterField = Constants.FIELD_ID;
                            reqConfigAccountLocation.objectName = Constants.OBJ_ACCOUNTLOCATION;
                            reqConfigAccountLocation.select = Constants.OBJ_ACCOUNTLOCATION_SELECTFIELD.Split(Constants.CHAR_COMMA).ToArray();

                            List<Dictionary<string, object>> contentPrimaryAddress = new List<Dictionary<string, object>>();
                            var dic = new Dictionary<string, object>();
                            dic.Add(Constants.FIELD_ID, objPrimaryAddId.Id);
                            contentPrimaryAddress.Add(dic);
                            var resMessageAccountLocation = Utilities.Search(contentPrimaryAddress, reqConfigAccountLocation);
                            var primaryAddressModel = JObject.Parse(resMessageAccountLocation.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToObject<List<AddressModel>>().FirstOrDefault();
                            if (primaryAddressModel != null)
                            {
                                primaryAddressModel.sourceSystem = Constants.CUSTOM_SYNC_SOURCESYSTEM;
                                primaryAddressModel.addressTypeCode = Constants.FIELD_ADDRESSTYPECODE;
                                primaryAddressModel.sendToSAPIndicator = 0;
                                responseAccountModel.addresses = new List<AddressModel>();
                                responseAccountModel.addresses.Add(primaryAddressModel);
                            }
                        }

                        objResponse = new JavaScriptSerializer().Serialize(responseAccountModel);
                        var accountResponse = HttpActions.PostRequestMule(Utilities.GetMuleHeaderDetail(muleXClientId, accountUrl, objResponse, muleXSourceSystem, accountAPIName, accountXBatchID));
                        if (accountResponse.IsSuccessStatusCode)
                        {
                            #region UpsertAccountData
                            List<Dictionary<string, object>> dictContentUpsertAccount = new List<Dictionary<string, object>>();
                            var id = dictContent.Where(data => data.ContainsKey(Constants.FIELD_ID)
                                        && !string.IsNullOrEmpty(Convert.ToString(data[Constants.FIELD_ID]))).Select(data => Convert.ToString(data[Constants.FIELD_ID])).FirstOrDefault();
                            if (!string.IsNullOrEmpty(id))
                                dictContentUpsertAccount.Add(new Dictionary<string, object> { { Constants.FIELD_ID, id } });
                                //HArdCoded ExtId For TEsting
                                //dictContentUpsertAccount.Add(new Dictionary<string, object> { { Constants.FIELD_ID, id }, { Constants.FIELD_EXTERNALID, id + "_Updated" } });

                            //update customer since in account
                            if (eventType == Constants.FIELD_UPDATECUSTOMEREVENTTYPE)
                            {
                                dictContent.Add(new Dictionary<string, object> { { Constants.FIELD_CUSTOMERSINCE, DateTime.Now } });
                            }
                            var response = Utilities.UpdateMuleResponse(dictContentUpsertAccount, Constants.OBJ_ACCOUNT);
                            return Utilities.CreateResponse(response);

                            #endregion
                        }
                        else
                        {
                            return Utilities.CreateResponse(accountResponse);
                        }

                        return accountResponse;
                    }
                }
                return null;
            }
            catch
            {
                //TODO
                throw;
            }
        }
        /// <summary>
        /// Use for Key contact role Sync on accountid
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        public HttpResponseMessage SendKeyContactRoleDataToMule(List<Dictionary<string, object>> dictContent)
        {
            try
            {
                string resAccountString = string.Empty;
                string resContactString = string.Empty;
                dictContent = Utilities.GetCaseIgnoreDictContent(dictContent);

                var requestConfigContact = Utilities.GetRequestConfiguration(Constants.OBJ_CONTACT, Constants.FIELD_ID, Constants.FIELD_PARENTCUSTOMERID);
                var resMessageContact = Utilities.Search(dictContent, requestConfigContact);
                if (resMessageContact.IsSuccessStatusCode)
                {
                    var keyContactListObject = JObject.Parse(resMessageContact.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToObject<List<AccountKeyRoleApttus>>().ToList();
                    var objContact = BindResponseAccountKeyRoleModel(keyContactListObject);
                    var contacts = new KeyContactRoles();
                    contacts.contacts = new List<AccountKeyRole>();
                    contacts.contacts.AddRange(objContact);
                    contacts.contacts.Select(x => x.roles.ToString());
                    var keyContactJson = new JavaScriptSerializer().Serialize(contacts);

                    //Get Id and ExternalID
                    var reqConfig = new RequestConfigModel();
                    reqConfig.accessToken = Utilities.GetAuthToken();
                    reqConfig.select = new string[] { Constants.FIELD_ID, Constants.FIELD_EXTERNALID };
                    reqConfig.externalFilterField = Constants.FIELD_ID;
                    reqConfig.apttusFilterField = Constants.FIELD_ID;
                    reqConfig.objectName = Constants.OBJ_ACCOUNT;

                    var requestConfigAccountMDM = Utilities.Search(dictContent, reqConfig);

                    if (requestConfigAccountMDM.IsSuccessStatusCode)
                    {
                        var mdmAccount = JObject.Parse(requestConfigAccountMDM.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).FirstOrDefault().ToObject<AccountModel>();
                        resAccountString = mdmAccount.mdmId;

                        MuleHeaderModel objMuleHeaderModel = new MuleHeaderModel();
                        objMuleHeaderModel.APIName = string.Format(ConfigurationManager.AppSettings[Constants.CONFIG_MOCKKEYCONTACTROLEAPI], resAccountString);
                        objMuleHeaderModel.MuleUrl = ConfigurationManager.AppSettings[Constants.CONFIG_MOCKAPIURL];
                        objMuleHeaderModel.HttpContentStr = new StringContent(keyContactJson, Encoding.UTF8, "application/json");
                        objMuleHeaderModel.AccessToken = Utilities.GetMuleAuthToken();
                        var httpMuleResponseMessage = HttpActions.PostRequestMule(objMuleHeaderModel);
                        //HardCoded Test
                        if(httpMuleResponseMessage.IsSuccessStatusCode)
                        {
                            List<Dictionary<string, object>> dictContentUpsertAccount = new List<Dictionary<string, object>>();
                            var id = dictContent.Where(data => data.ContainsKey(Constants.FIELD_ID)
                                        && !string.IsNullOrEmpty(Convert.ToString(data[Constants.FIELD_ID]))).Select(data => Convert.ToString(data[Constants.FIELD_ID])).FirstOrDefault();
                            if (!string.IsNullOrEmpty(id))
                                dictContentUpsertAccount.Add(new Dictionary<string, object> { { Constants.FIELD_ID, id }, { Constants.FIELD_EXTERNALID, id + "_UpdatedKeyContact" } });
                            var response = Utilities.UpdateMuleResponse(dictContentUpsertAccount, Constants.OBJ_ACCOUNT);
                        }
                        //////////////
                        return httpMuleResponseMessage;
                    }
                    else
                        return requestConfigAccountMDM;
                }
                return Utilities.CreateResponse(resMessageContact);
            }
            catch (Exception)
            {
                //TODO
                throw;
            }
        }

        /// <summary>
        /// Bind Object of AccountKeyRole
        /// </summary>
        /// <param name="assetLineItemData"></param>
        /// <returns></returns>
        public List<AccountKeyRole> BindResponseAccountKeyRoleModel(List<AccountKeyRoleApttus> listContactData)
        {
            List<AccountKeyRole> objListRole = new List<AccountKeyRole>();
            foreach (var Data in listContactData)
            {
                AccountKeyRole objRole = new AccountKeyRole();
                objRole.id = Data.ExternalId;
                objRole.roles = Data.ext_KeyContactRole != null && Data.ext_KeyContactRole.Count() > 0 ? Data.ext_KeyContactRole[0] : "";
                objListRole.Add(objRole);
            }
            return objListRole;
        }
        #endregion


    }
}