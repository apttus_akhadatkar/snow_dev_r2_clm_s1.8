﻿using Apttus.DataAccess.Common.Enums;
using Apttus.DataAccess.Common.Model;
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apttus.SNowPS.Repository
{
    public class AssetLineItemRepository
    {
        /// <summary>
        /// Get AssetLineItem Detail
        /// </summary>
        /// <param name="AssetLineItemId"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public string GetAssetLineItemDetail(string AssetLineItemId, string accessToken)
        {
            var quoteJson = string.Empty;
            //Build AQL Query to fetch AssetLineItem
            try
            {
                var query = new Query(Constants.OBJ_ASSETLINEITEM);
                query.AddColumns(Constants.OBJ_ASSETLINEITEM_SELECTFIELD.Split(Constants.CHAR_COMMA));
                Expression exp = new Expression(ExpressionOperator.AND);
                exp.AddCondition(new Condition(Constants.FIELD_ID, FilterOperator.Equal, AssetLineItemId));

                Join AgreementLineItem = new Join(Constants.OBJ_ASSETLINEITEM, Constants.OBJ_AGREEMENTLINEITEM, Constants.FIELD_BUSINESSLINEITEMID, Constants.FIELD_ID, JoinType.LEFT);
                AgreementLineItem.EntityAlias = Constants.FIELD_BUSINESSLINEITEMID;
                query.AddJoin(AgreementLineItem);
                Join AgreementNumber = new Join(Constants.FIELD_BUSINESSLINEITEMID, Constants.OBJ_AGREEMENT, Constants.FIELD_AGREEMENTID, Constants.FIELD_ID, JoinType.LEFT);
                AgreementNumber.EntityAlias = Constants.FIELD_AGREEMENTID;
                query.AddJoin(AgreementNumber);

                query.SetCriteria(exp);
                var jsonQuery = query.Serialize();

                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.searchType = SearchType.AQL;
                reqConfig.objectName = Constants.OBJ_ASSETLINEITEM;

                var response = Utilities.Search(jsonQuery, reqConfig);
                if (response != null && response.IsSuccessStatusCode)
                {
                    var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToString();
                    var assetData = JsonConvert.DeserializeObject<List<ResponseAssetLineItem>>(responseString);

                    //Get json request for Mule
                    if (assetData != null && assetData.Count > 0)
                        quoteJson = BindResponseAgreementModel(assetData.First());
                }
                return quoteJson;
            }
            catch (Exception ex)
            {
                //TODO
                return string.Empty;
            }
        }

        /// <summary>
        /// Bind Object of AssetlineItem
        /// </summary>
        /// <param name="assetLineItemData"></param>
        /// <returns></returns>
        public string BindResponseAgreementModel(ResponseAssetLineItem assetLineItemData)
        {
            AssetLineItemModel objAsset = new AssetLineItemModel();
            objAsset.id = assetLineItemData.Id;
            objAsset.startDate = assetLineItemData.StartDate;
            objAsset.endDate = assetLineItemData.EndDate;
            objAsset.dataCenter = assetLineItemData.ext_Datacenter;
            objAsset.physicalDataCenter = assetLineItemData.ext_PhysicalDataCentre;
            objAsset.parentAgreementNumber = assetLineItemData.ext_ServiceNowContractNumber;
            objAsset.productID = assetLineItemData.ProductId != null ? assetLineItemData.ProductId.Id : null;
            objAsset.agreementID = assetLineItemData.ext_AgreementId != null ? assetLineItemData.ext_AgreementId.Id : null;
            objAsset.productName = assetLineItemData.ProductId != null ? assetLineItemData.ProductId.Name : Constants.STR_NONE;
            objAsset.optionId = assetLineItemData.OptionId != null ? assetLineItemData.OptionId.Id : null;
            objAsset.optionName = assetLineItemData.OptionId != null ? assetLineItemData.OptionId.Name : null;
            objAsset.parentassetLineNumber = assetLineItemData.ParentBundleNumber;
            objAsset.itemSequence = assetLineItemData.ItemSequence;
            objAsset.assetLineNumber = assetLineItemData.LineNumber;
            objAsset.primaryLineNumber = assetLineItemData.PrimaryLineNumber;
            objAsset.netPrice = assetLineItemData.NetPrice;
            objAsset.quantity = assetLineItemData.Quantity;
            objAsset.releaseVersion = assetLineItemData.ext_ReleaseVersion;
            objAsset.hiChgRequestNumber = assetLineItemData.ext_ChangeTicket;
            objAsset.instanceCreationDate = assetLineItemData.ext_InstanceCreatedOn.HasValue ? assetLineItemData.ext_InstanceCreatedOn : null;
            objAsset.instanceId = assetLineItemData.ext_DCInstanceID;
            objAsset.instanceName = assetLineItemData.ext_InstanceName;
            objAsset.instanceMappingID = assetLineItemData.ext_InstalledMappingID;
            objAsset.instanceMappingName = assetLineItemData.ext_InstalledMappingName;

            objAsset.annualContractValue = assetLineItemData.ext_AnnualContractValue;
            objAsset.annualContractValueInUSD = assetLineItemData.ext_USDAnnualContractValue;
            objAsset.annualListPrice = assetLineItemData.ext_AnnualListPrice;
            objAsset.annualListPriceInUSD = assetLineItemData.ext_USDAnnualListPrice;
            objAsset.businessUnit = assetLineItemData.ext_BusinessUnit;
            objAsset.adjustmentInUSD = assetLineItemData.ext_USDAdjustment;
            objAsset.basePriceInUSD = assetLineItemData.ext_USDBasePrice;
            objAsset.listPriceInUSD = assetLineItemData.ext_USDListPrice;
            objAsset.totalValueInUSD = assetLineItemData.ext_USDTotalValue;
            objAsset.salesPriceInUSD = assetLineItemData.ext_USDSalesPrice;
            objAsset.allocationPercentage = assetLineItemData.ext_AllocationPercent;
            objAsset.deploymentNumber = assetLineItemData.ext_Deployment;
            objAsset.icLevel = assetLineItemData.ext_ICLevel!=null? assetLineItemData.ext_ICLevel.Key:null;
            objAsset.practice = assetLineItemData.ext_Practice!=null? assetLineItemData.ext_Practice.Key:null;
            objAsset.resourcePlan = assetLineItemData.ext_ResourcePlan;
            objAsset.areResourceRolesFixedFeeItems = assetLineItemData.ext_ResourceRolesFixedFeeItems;
            objAsset.soldMappingID = assetLineItemData.ext_SoldMappingID;
            objAsset.soldMappingName = assetLineItemData.ext_SoldMappingName;
            objAsset.taskID = assetLineItemData.ext_TaskID;
            objAsset.contractNumber = assetLineItemData.ext_ServiceNowContractNumber;

            objAsset.operationalStatus = assetLineItemData.ext_OperationStatus;
            objAsset.name = assetLineItemData.Name;
            objAsset.adjustedPrice = assetLineItemData.AdjustedPrice;
            objAsset.actionsAllowed = assetLineItemData.AllowedActions!=null && assetLineItemData.AllowedActions.Count()>0 ? string.Join(",", assetLineItemData.AllowedActions):"";
            objAsset.assetARR = assetLineItemData.AssetARR;
            objAsset.assetcode = assetLineItemData.AssetCode;
            objAsset.assetMRR = assetLineItemData.AssetMRR;
            objAsset.assetNumber = assetLineItemData.AssetNumber;
            objAsset.status = assetLineItemData.AssetStatus!=null? assetLineItemData.AssetStatus.Key:null;
            objAsset.tcv = assetLineItemData.AssetTCV;
            objAsset.attributeValueID = assetLineItemData.AttributeValueId!=null?assetLineItemData.AttributeValueId.Id:null;
            objAsset.isAutoRenew = assetLineItemData.AutoRenew;
            objAsset.autoRenewalType = assetLineItemData.AutoRenewalType!=null? assetLineItemData.AutoRenewalType.Key:null;
            objAsset.availableBalance = assetLineItemData.AvailableBalance;
            objAsset.baseCost = assetLineItemData.BaseCost;
            objAsset.baseCostExtended = assetLineItemData.BaseExtendedCost;
            objAsset.basePriceExtended = assetLineItemData.BaseExtendedPrice;
            objAsset.basePriceMethod = assetLineItemData.BasePriceMethod!=null? assetLineItemData.BasePriceMethod.Key:null;
            objAsset.basePrice = assetLineItemData.BasePrice;
            objAsset.billThroughDate = assetLineItemData.BillThroughDate;
            objAsset.billToMDMID = assetLineItemData.ext_billToMDMID;
            objAsset.billingEndDate = assetLineItemData.BillingEndDate;
            objAsset.billingFrequency = assetLineItemData.BillingFrequency!=null? assetLineItemData.BillingFrequency.Key:null;
            objAsset.billingPlanID = assetLineItemData.BillingPlanId!=null?assetLineItemData.BillingPlanId.Id:null;
            objAsset.billingPreferenceID = assetLineItemData.BillingPreferenceId!=null?assetLineItemData.BillingPreferenceId.Id:null;
            objAsset.billingRule = assetLineItemData.BillingRule!=null? assetLineItemData.BillingRule.Key:null;
            objAsset.billingStartDate = assetLineItemData.BillingStartDate;
            objAsset.bundleLineItemID = assetLineItemData.BundleAssetId!=null? assetLineItemData.BundleAssetId.Id:null;
            objAsset.busineesLineItemID = assetLineItemData.BusinessLineItemId;
            objAsset.businessObjectID = assetLineItemData.BusinessObjectId;
            objAsset.busineesObjectType = assetLineItemData.BusinessObjectType!=null? assetLineItemData.BusinessObjectType.Key:null;
            objAsset.cancellationDate = assetLineItemData.CancelledDate;
            objAsset.chargeType = assetLineItemData.ChargeType!=null? assetLineItemData.ChargeType.Key:null;
            objAsset.comments = assetLineItemData.Comments;
            objAsset.deltaPrice = assetLineItemData.DeltaPrice;
            objAsset.deltaQuantity = assetLineItemData.DeltaQuantity;
            objAsset.description = assetLineItemData.Description;
            objAsset.extendedCost = assetLineItemData.ExtendedCost;
            objAsset.extendedDescription = assetLineItemData.ExtendedDescription;
            objAsset.extendedPrice = assetLineItemData.ExtendedPrice;
            objAsset.pricingfrequencyType = assetLineItemData.Frequency!=null? assetLineItemData.Frequency.Key:null;
            objAsset.hasAttributes = assetLineItemData.HasAttributes;
            objAsset.hasOptions = assetLineItemData.HasOptions;
            objAsset.hideInvoiceDisplay = assetLineItemData.HideInvoiceDisplay;
            objAsset.initialActivationDate = assetLineItemData.InitialActivationDate;
            objAsset.isOptionRoleUpLine = assetLineItemData.IsOptionRollupLine;
            objAsset.isPrimaryLine = assetLineItemData.IsPrimaryLine;
            objAsset.isPrimaryRampLine = assetLineItemData.IsPrimaryRampLine;
            objAsset.isReadOnly = assetLineItemData.IsReadOnly;
            objAsset.isRenewalPending = assetLineItemData.IsRenewalPending;
            objAsset.isRenewed = assetLineItemData.IsRenewed;
            objAsset.isUsageTierModifiable = assetLineItemData.IsUsageTierModifiable;
            objAsset.lastRenewEndDate = assetLineItemData.LastRenewEndDate;
            objAsset.lineType = assetLineItemData.LineType!=null?assetLineItemData.LineType.Key:null;
            objAsset.listPrice = assetLineItemData.ListPrice;
            objAsset.locationID = assetLineItemData.LocationId!=null? assetLineItemData.LocationId.Id:null;
            objAsset.maxUsageQuantity = assetLineItemData.MaxUsageQuantity;
            objAsset.minUsageQuantity = assetLineItemData.MinUsageQuantity;
            objAsset.mustUpgrade = assetLineItemData.MustUpgrade;
            objAsset.netUnitPrice = assetLineItemData.NetUnitPrice;
            objAsset.nextRenewalEndDate = assetLineItemData.NextRenewEndDate;
            objAsset.optionCost = assetLineItemData.OptionCost;
            objAsset.optionPrice = assetLineItemData.OptionPrice;
            objAsset.originalStartDate = assetLineItemData.OriginalStartDate;
            objAsset.parentAssetID = assetLineItemData.ParentAssetId!=null? assetLineItemData.ParentAssetId.Id:null;
            objAsset.paymentTermID = assetLineItemData.PaymentTermId!=null? assetLineItemData.PaymentTermId.Id:null;
            objAsset.priceGroup = assetLineItemData.PriceGroup!=null? assetLineItemData.PriceGroup.Key:null;
            objAsset.isPriceIncluded = assetLineItemData.PriceIncludedInBundle;
            objAsset.priceListID = assetLineItemData.PriceListId!=null? assetLineItemData.PriceListId.Id:null;
            objAsset.priceListItemID = assetLineItemData.PriceListItemId!=null? assetLineItemData.PriceListItemId.Id:null;
            objAsset.priceMethodType = assetLineItemData.PriceMethod!=null? assetLineItemData.PriceMethod.Key:null;
            objAsset.priceType = assetLineItemData.PriceType!=null? assetLineItemData.PriceType.Key:null;
            objAsset.priceUOM = assetLineItemData.PriceUom!=null? assetLineItemData.PriceUom.Key:null;
            objAsset.pricingDate = assetLineItemData.PricingDate;
            objAsset.productType = assetLineItemData.ProductType!=null? assetLineItemData.ProductType.Key:null;
            objAsset.purchaseDate = assetLineItemData.PurchaseDate;
            objAsset.renewalAdjustmentAmount = assetLineItemData.RenewalAdjustmentAmount;
            objAsset.renewalAdjustmentType = assetLineItemData.RenewalAdjustmentType!=null? assetLineItemData.RenewalAdjustmentType.Key:null;
            objAsset.renewalDate = assetLineItemData.RenewalDate;
            objAsset.renewalFrequencyType = assetLineItemData.RenewalFrequency!=null? assetLineItemData.RenewalFrequency.Key:null;
            objAsset.renewalTerm = assetLineItemData.RenewalTerm;
            objAsset.sellingFrequencyType = assetLineItemData.SellingFrequency!=null? assetLineItemData.SellingFrequency.Key:null;
            objAsset.sellingTerm = assetLineItemData.SellingTerm;
            objAsset.shipToMDMID = assetLineItemData.ext_shipToMDMID;
            objAsset.taxCodeID = assetLineItemData.TaxCodeId!=null? assetLineItemData.TaxCodeId.Id:null;
            objAsset.isTaxInclusive = assetLineItemData.TaxInclusive;
            objAsset.isTaxable = assetLineItemData.Taxable;
            objAsset.term = assetLineItemData.Term;
            objAsset.totalBalance = assetLineItemData.TotalBalance;
            objAsset.quoteID = assetLineItemData.QuoteId!=null? assetLineItemData.QuoteId.Id:null;
            objAsset.quoteLineItemID = assetLineItemData.QuoteLineItemId != null ? assetLineItemData.QuoteLineItemId.Id : null;


            AssetMuleRequest objMuleAssetLine = new AssetMuleRequest();
            objMuleAssetLine.mdmID = assetLineItemData.ext_AccountMDMId;
            objMuleAssetLine.accountName = assetLineItemData.AccountId != null ? assetLineItemData.AccountId.Name : null;
            List<AssetLineItemModel> lstAsset = new List<AssetLineItemModel>();
            lstAsset.Add(objAsset);
            objMuleAssetLine.lineItems = lstAsset;
            //Get json
            return JsonConvert.SerializeObject(objMuleAssetLine);
        }

    }
}
