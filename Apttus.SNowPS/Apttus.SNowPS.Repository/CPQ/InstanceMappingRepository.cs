﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Apttus.SNowPS.Model;
using Apttus.SNowPS.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Script.Serialization;

namespace Apttus.SNowPS.Respository
{
    public static class InstanceMappingRepository
    {
        public static List<LineItem> GetLineItems(string configId, string accessToken)
        {
            List<LineItem> lstLItems = new List<LineItem>();
            try
            {
                if (!String.IsNullOrEmpty(configId))
                {                    
                    var reqConfig = new RequestConfigModel();
                    reqConfig.accessToken = accessToken;
                    reqConfig.objectName = Constants.OBJ_CPQ_LINEITEM;                                        
                    reqConfig.select = new string[] { Constants.FIELD_NAME, Constants.FIELD_EXT_INSTANCE_NAME, Constants.FIELD_NAME, Constants.FIELD_PRODUCTID, Constants.FIELD_LINETYPE,Constants.FIELD_EXT_LINumInstMap, Constants.FIELD_EXT_SOLDLINAMEINSTMAP, Constants.FIELD_EXT_INSTALLEDLINAMEINSTMAP, Constants.FIELD_EXT_SOLDLINUMINSTMAP, Constants.FIELD_EXT_INSTALLEDLINUMINSTMAP };
                    //reqConfig.externalFilterField = "TerritoryMDMID";
                    reqConfig.apttusFilterField =  Constants.FIELD_CONFIGURATIONID; //Territory MDM ID

                    var searchAPI = Convert.ToString(ConfigurationManager.AppSettings[Constants.CONFIG_GETSEARCHAPI]);
                    var appUrl = Convert.ToString(ConfigurationManager.AppSettings[Constants.CONFIG_APPURL]);
                    string appInstance = Convert.ToString(ConfigurationManager.AppSettings[Constants.CONFIG_APPINSTANCE]);

                    RequestModel requestModel = new RequestModel();
                    requestModel.logicalExpression = null;
                    requestModel.select = reqConfig.select;
                    requestModel.filter = new List<FilterModel>();
                    
                    string[] filterValue = new string[] { configId.ToString() };
                    requestModel.filter.Add(new FilterModel { field = reqConfig.apttusFilterField, @operator = Constants.OPR_EQUAL, value = filterValue });

                    var jsonQuery = new JavaScriptSerializer().Serialize(requestModel);
                    var responseString = Utilities.GetSearch(jsonQuery, reqConfig);

                    if (responseString.IsSuccessStatusCode)
                    {
                        string responseJSON = responseString.Content.ReadAsStringAsync().Result.ToString();
                        ResponseModel jsonResp = new JavaScriptSerializer().Deserialize<ResponseModel>(responseJSON);

                        lstLItems = MapRespToLI(jsonResp.SerializedResultEntities, accessToken);
                    }

                    return lstLItems;
                    //respContacts = Utilities.Utilities.Update(dictContent, reqConfig);
                }

                else
                    return null;
            }

            catch (Exception ex)
            {
                throw;
            }
        }

        public static Product GetProductInfo(string productId, string accessToken)
        {
            Product product = new Product();
            try
            {
                if (!String.IsNullOrEmpty(productId))
                {
                    //Update Address
                    var reqConfig = new RequestConfigModel();
                    reqConfig.accessToken = accessToken;
                    reqConfig.objectName = Constants.OBJ_PRODUCT;                    
                    reqConfig.select = new string[] { Constants.FIELD_NAME, Constants.FIELD_ID, Constants.FIELD_EXT_FAMILY1, Constants.FIELD_EXT_FAMILY};
                    //reqConfig.externalFilterField = "TerritoryMDMID";
                    reqConfig.apttusFilterField = Constants.FIELD_ID;

                    var searchAPI = Convert.ToString(ConfigurationManager.AppSettings[Constants.CONFIG_GETSEARCHAPI]);
                    var appUrl = Convert.ToString(ConfigurationManager.AppSettings[Constants.CONFIG_APPURL]);
                    string appInstance = Convert.ToString(ConfigurationManager.AppSettings[Constants.CONFIG_APPINSTANCE]);

                    RequestModel requestModel = new RequestModel();
                    requestModel.logicalExpression = null;
                    requestModel.select = reqConfig.select;
                    requestModel.filter = new List<FilterModel>();
                    
                    string[] filterValue = new string[] { productId.ToString() };
                    requestModel.filter.Add(new FilterModel { field = reqConfig.apttusFilterField, @operator = Constants.OPR_EQUAL, value = filterValue });

                    var jsonQuery = new JavaScriptSerializer().Serialize(requestModel);
                    var responseString = Utilities.GetSearch(jsonQuery, reqConfig);

                    if (responseString.IsSuccessStatusCode)
                    {
                        string responseJSON = responseString.Content.ReadAsStringAsync().Result.ToString();
                        ResponseModel jsonResp = new JavaScriptSerializer().Deserialize<ResponseModel>(responseJSON);

                        product = MapRespToProduct(jsonResp.SerializedResultEntities);
                    }                               
                }
                return product;
              
            }

            catch (Exception ex)
            {
                throw;
            }
        }        

        public static List<CustomerInstanceModel> GetCustomerInstances(string accessToken)
        {
            {

                List<CustomerInstanceModel> instanceList = new List<CustomerInstanceModel>();
                HttpResponseMessage mymsg = new HttpResponseMessage();

                try
                {
                    //var accessToken = Utilities.GetAuthToken();

                    var reqConfig = new RequestConfigModel
                    {
                        accessToken = accessToken,
                        objectName = Constants.OBJ_EXT_CUSTOMERINSTANCE,
                        select = new string[] {Constants.FIELD_NAME, Constants.FIELD_ID },                        
                    };

                    var searchAPI = Convert.ToString(ConfigurationManager.AppSettings[Constants.CONFIG_GETSEARCHAPI]);
                    var appUrl = Convert.ToString(ConfigurationManager.AppSettings[Constants.CONFIG_APPURL]);
                    string appInstance = Convert.ToString(ConfigurationManager.AppSettings[Constants.CONFIG_APPINSTANCE]);

                    RequestModel requestModel = new RequestModel
                    {
                        select = reqConfig.select,
                        filter = new List<FilterModel>(),
                        logicalExpression = null
                    };

                    var jsonQuery = new JavaScriptSerializer().Serialize(requestModel);
                    var responseString = Utilities.GetSearch(jsonQuery, reqConfig);

                    if (responseString.IsSuccessStatusCode)
                    {
                        string responseJSON = responseString.Content.ReadAsStringAsync().Result.ToString();
                        ResponseModel jsonResp = new JavaScriptSerializer().Deserialize<ResponseModel>(responseJSON);

                        foreach (var y in jsonResp.SerializedResultEntities)
                        {
                            CustomerInstanceModel inst = new CustomerInstanceModel();
                            inst.Name = y[Constants.FIELD_NAME];
                            inst.Id = y[Constants.FIELD_ID];
                            instanceList.Add(inst);
                        }
                    }
                                      
                    return instanceList;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        private static List<LineItem> MapRespToLI(dynamic[] deserializedEntity, string accessToken)
        {
            List<LineItem> lineItems = new List<LineItem>();
            foreach (dynamic obj in deserializedEntity)
            {
                LineItem lineItem = new LineItem();
                lineItem.Id = obj[Constants.FIELD_ID];
                lineItem.Name = obj[Constants.FIELD_NAME];
                lineItem.InstanceName = obj[Constants.FIELD_EXT_INSTANCE_NAME];
                if (obj[Constants.FIELD_PRODUCTID] != null)
                {
                    lineItem.ProductId = obj[Constants.FIELD_PRODUCTID][Constants.FIELD_ID];
                    lineItem.Product = GetProductInfo(obj[Constants.FIELD_PRODUCTID][Constants.FIELD_ID], accessToken);
                }
                if (obj[Constants.FIELD_LINETYPE] !=null)
                    lineItem.LineType = obj[Constants.FIELD_LINETYPE][Constants.FIELD_VALUE];
                if (obj[Constants.FIELD_EXT_LINumInstMap] != null)
                    lineItem.LINumInstMap = obj[Constants.FIELD_EXT_LINumInstMap];
                if (obj[Constants.FIELD_EXT_SOLDLINAMEINSTMAP] !=null)
                {
                    string lstOfSoldProducts = String.Empty; ;
                    lstOfSoldProducts = obj[Constants.FIELD_EXT_SOLDLINAMEINSTMAP];
                    lineItem.SoldLINameInstMap = !String.IsNullOrEmpty(lstOfSoldProducts) ? lstOfSoldProducts.Split(',').ToList() : null;
                }

                if (obj[Constants.FIELD_EXT_SOLDLINUMINSTMAP] != null)
                {
                    string lstOfSoldProductsNumbers = String.Empty; ;
                    lstOfSoldProductsNumbers = obj[Constants.FIELD_EXT_SOLDLINUMINSTMAP];
                    lineItem.SoldLINumInstMap = !String.IsNullOrEmpty(lstOfSoldProductsNumbers) ? lstOfSoldProductsNumbers.Split(',').ToList() : null;
                }

                if (obj[Constants.FIELD_EXT_INSTALLEDLINAMEINSTMAP] != null)
                {
                    string lstOfInstalledProducts = String.Empty;
                    lstOfInstalledProducts = obj[Constants.FIELD_EXT_INSTALLEDLINAMEINSTMAP];
                    lineItem.InstalledLINameInstMap = !String.IsNullOrEmpty(lstOfInstalledProducts) ? lstOfInstalledProducts.Split(',').ToList() : null;
                }

                if (obj[Constants.FIELD_EXT_INSTALLEDLINUMINSTMAP] != null)
                {
                    string lstOfInstalledProductsNumbers = String.Empty;
                    lstOfInstalledProductsNumbers = obj[Constants.FIELD_EXT_INSTALLEDLINUMINSTMAP];
                    lineItem.InstalledLINumInstMap = !String.IsNullOrEmpty(lstOfInstalledProductsNumbers) ? lstOfInstalledProductsNumbers.Split(',').ToList() : null;
                }

                lineItems.Add(lineItem);
            }
            return lineItems;
        }

        private static Product MapRespToProduct(dynamic[] deserializedEntity)
        {            
            Product product = new Product();
            deserializedEntity.ToList().ForEach(x =>
            {
                product.Id = x[Constants.FIELD_ID];
                product.Name = x[Constants.FIELD_NAME];

                if (x[Constants.FIELD_EXT_FAMILY] != null)
                {
                    product.ExtFamily = new SelectOption();
                    product.ExtFamily.Key = x[Constants.FIELD_EXT_FAMILY][Constants.FIELD_KEY];
                    product.ExtFamily.Value = x[Constants.FIELD_EXT_FAMILY][Constants.FIELD_VALUE];
                }

                product.Family1 = new SelectOption();
                if (x[Constants.FIELD_EXT_FAMILY1] != null)
                {                    
                    product.Family1.Key = x[Constants.FIELD_EXT_FAMILY1][Constants.FIELD_KEY];
                    product.Family1.Value = x[Constants.FIELD_EXT_FAMILY1][Constants.FIELD_VALUE];
                }
            });
            return product;            
        }   
        
        public static List<Dictionary<string,object>> BuildSKUMappingtoUpdate(ref List<InstanceModel> refInstances)
        {
            try
            {
                List<Dictionary<string, object>> upsValues = new List<Dictionary<string, object>>();
                List<InstanceModel> instances = refInstances;
                //Build the instSKUs data to update
                instances.ForEach(i =>
                {
                    i.SoldOnProducts = new List<string>();
                    i.SoldOnProductsNumbers = new List<string>();
                    i.Products.ForEach(p =>
                    {
                        if (p.SoldOn)
                        {
                        //i.SoldOnProducts.Add(p.prodLIName);
                        i.SoldOnProducts.Add(p.Name);
                            i.SoldOnProductsNumbers.Add(p.prodLINumInstMap);
                        }
                    });

                    i.InstalledOnProducts = new List<string>();
                    i.InstalledOnProductsNumbers = new List<string>();
                    i.Products.ForEach(p =>
                    {
                        if (p.InstalledOn)
                        {
                        //i.InstalledOnProducts.Add(p.prodLIName);
                        i.InstalledOnProducts.Add(p.Name);
                            i.InstalledOnProductsNumbers.Add(p.prodLINumInstMap);
                        }
                    });

                    Dictionary<string, object> instSKUsToUpdate = new Dictionary<string, object>();
                    instSKUsToUpdate.Add(Constants.FIELD_ID, i.instLIId);
                    instSKUsToUpdate.Add(Constants.FIELD_EXT_INSTANCE_NAME, i.instName);
                    instSKUsToUpdate.Add(Constants.FIELD_EXT_SOLDLINAMEINSTMAP, String.Join(",", i.SoldOnProducts));
                    instSKUsToUpdate.Add(Constants.FIELD_EXT_INSTALLEDLINAMEINSTMAP, String.Join(",", i.InstalledOnProducts));
                    instSKUsToUpdate.Add(Constants.FIELD_EXT_SOLDLINUMINSTMAP, String.Join(",", i.SoldOnProductsNumbers));
                    instSKUsToUpdate.Add(Constants.FIELD_EXT_INSTALLEDLINUMINSTMAP, String.Join(",", i.InstalledOnProductsNumbers));

                    upsValues.Add(instSKUsToUpdate);
                });

                //Build the prodSKUs data to update. Looping only for the first instance will do because the list of prodSKUs wont differ for the instances
                if (instances.Count > 0)
                {
                    instances[0].Products.ForEach(p =>
                    {
                        List<string> soldOnInstances = new List<string>();
                        List<string> installedOnInstances = new List<string>();

                        List<string> soldOnInstancesNumbers = new List<string>();
                        List<string> installedOnInstancesNumbers = new List<string>();
                        string prodInstanceName = String.Empty;

                        instances.ForEach(i =>
                        {
                            if (i.IsProdInstance)
                                prodInstanceName = i.instName;
                            InstanceProduct prod = i.Products.Find(pi => pi.Id == p.Id);
                            if (prod.SoldOn)
                            {
                            //soldOnInstances.Add(i.Name);
                            soldOnInstances.Add(i.instName);
                                soldOnInstancesNumbers.Add(i.instLINumInstMap);
                            }

                            if (prod.InstalledOn)
                            {
                            //installedOnInstances.Add(i.Name);
                            installedOnInstances.Add(i.instName);
                                installedOnInstancesNumbers.Add(i.instLINumInstMap);
                            }
                        });

                        Dictionary<string, object> prodSKUsToUpdate = new Dictionary<string, object>();
                        prodSKUsToUpdate.Add(Constants.FIELD_ID, p.prodLIId);
                        prodSKUsToUpdate.Add(Constants.FIELD_EXT_INSTANCE_NAME, prodInstanceName);
                        prodSKUsToUpdate.Add(Constants.FIELD_EXT_SOLDLINAMEINSTMAP, String.Join(",", soldOnInstances));
                        prodSKUsToUpdate.Add(Constants.FIELD_EXT_INSTALLEDLINAMEINSTMAP, String.Join(",", installedOnInstances));
                        prodSKUsToUpdate.Add(Constants.FIELD_EXT_SOLDLINUMINSTMAP, String.Join(",", soldOnInstancesNumbers));
                        prodSKUsToUpdate.Add(Constants.FIELD_EXT_INSTALLEDLINUMINSTMAP, String.Join(",", installedOnInstancesNumbers));

                        upsValues.Add(prodSKUsToUpdate);
                    });
                }
                return upsValues;
            }

            catch(Exception ex)
            {
                throw ex;
            }
        }
        
    }
}