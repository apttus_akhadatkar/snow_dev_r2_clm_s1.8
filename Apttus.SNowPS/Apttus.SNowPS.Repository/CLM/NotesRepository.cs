﻿using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apttus.SNowPS.Repository.CLM
{
    public class NotesRepository
    {
        public string AccessToken { get; set; }
        public string ObjectName { get; set; }

        public NotesRepository(string accessToken)
        {
            AccessToken = accessToken;
            ObjectName = Constants.OBJ_NOTES;
        }

        public string AddNote(List<Dictionary<string, object>> body, string entityName, string id)
        {
            var notes = new List<NoteModel>();
            foreach (var note in body)
            {
                var name = note["note"].ToString();
                var notetext = note["notetext"].ToString();
                notes.Add(new NoteModel()
                {
                    Name = name,
                    NoteText = notetext,
                    ContextObject = new ContextObject
                    {
                        Id = id,
                        Type = entityName
                    }
                });
            }
            

            var response = Utilities.Create(notes,
                            new Model.RequestConfigModel
                            {
                                accessToken = AccessToken,
                                objectName = ObjectName
                            });
            var result = response.Content.ReadAsStringAsync().Result.Trim('"');
            return result;
        }
    }
}
