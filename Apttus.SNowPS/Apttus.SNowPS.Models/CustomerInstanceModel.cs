﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apttus.SNowPS.Model
{
    public class CustomerInstanceModel
    {
        public String Id { get; set; }
        public String Name { get; set; }
    }
}