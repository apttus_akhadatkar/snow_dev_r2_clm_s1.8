﻿/****************************************************************************************
@Name: RequestModel.cs
@Author: Mahesh Patel
@CreateDate: 1 Sep 2017
@Description: Request related properties 
@UsedBy: This will be used as a entity for Request

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

using System.Collections.Generic;

namespace Apttus.SNowPS.Model
{
    public class RequestModel
    {
        public string[] select { get; set; }

        public List<FilterModel> filter { get; set; }

        public int offset { get; set; }

        public int limit { get; set; }

        public string logicalExpression { get; set; }

        public string crmType { get; set; }

        public string crmObject { get; set; }
    }

    public class FilterModel
    {
        public string field { get; set; }

        public string @operator { get; set; }

        public string[] value { get; set; }
    }

    public class AttributeRuleRequest
    {
        public string ProductId { get; set; }
        public int NumberOfRecordsPerChunk { get; set; }
    }

    public class AttributeRuleRequestModel
    {
        public AttributeRuleRequest requestModel { get; set; }
    }

    public class LineItemRequestModel
    {
        public string ProductConfigId { get; set; }
    }
}