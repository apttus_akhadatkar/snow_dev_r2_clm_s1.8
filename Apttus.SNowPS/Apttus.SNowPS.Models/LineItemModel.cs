﻿/****************************************************************************************
@Name: LineItemModel.cs
@Author: Rahul Der
@CreateDate: 11 Oct 2017
@Description: Line Item model
@UsedBy: This will be used for cart line item fileds

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Apttus.SNowPS.Model
{
    public class LineItemModel
    {
        public List<LineItemResponse> ListLineItems { get; set; }
    }

    #region "Classes used for Line Item Details in cart"

    public class LineItemsViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public ProductId ProductId { get; set; }
        public ChoiceType SellingFrequency { get; set; }

        public decimal? AdjustmentAmount { get; set; }
        public decimal? ListPrice { get; set; }
        public decimal? Quantity { get; set; }

        public decimal? Term { get; set; }
        public decimal? NetPrice { get; set; }
        public decimal? AdjustedPrice { get; set; }
        public decimal? ext_AnnualContractValue { get; set; }
        public decimal? ext_AnnualListPrice { get; set; }
        public decimal? ext_EstimatedTotal { get; set; }
        public decimal? ext_SalesPrice { get; set; }
        public ChoiceType AdjustmentType { get; set; }
        public string BillingFrequency { get; set; }
        public decimal? ext_TermNew { get; set; }
        
        public string RowVersion { get; set; }
        public decimal? NetUnitPrice { get; set; }
        public decimal? BasePrice { get; set; }
        public decimal? NetAdjustmentPercent { get; set; }
        public decimal? ext_EstimatedTax { get; set; }
        public ConfigurationId ConfigurationId { get; set; }
        public ChoiceType ext_FlaggedAs { get; set; }
        public string CurrencyId { get; set; }
        public decimal? ext_USDAdjustment { get; set; }
        public decimal? ext_USDBasePrice { get; set; }
        public decimal? ext_USDTotalValue { get; set; }
        public decimal? ext_USDAnnualListPrice { get; set; }
        public decimal? ext_USDListPrice { get; set; }
        public decimal? ext_USDSalesPrice { get; set; }
        public decimal? ext_USDAnnualContractValue { get; set; }
        public decimal? DeltaQuantity { get; set; }
        public decimal? ExtendedPrice { get; set; }
        public decimal? ext_TCV { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public PropductLineItemModel crm_Product { get; set; }
        public ProductConfiguration cpq_ProductConfiguration { get; set; }

        public QuoteDetailLineItem cpq_Quote { get; set; }
    }

    public class ProductId
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class PropductLineItemModel
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public ChoiceType ext_Family { get; set; }
    }

    public class SellingFrequency
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
    public class ConfigurationId
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class DailyExcnageRate
    {
        public string Name { get; set; }
        public string ext_CurrencyCode { get; set; }
        public DateTime? ExternalLastUpdatedOn { get; set; }
        public string ext_CurrencySymbol { get; set; }
        public decimal ext_Rate { get; set; }
        public DateTime ModifiedOn { get; set; }

    }

    public class UnitOfMeasure
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class ProductConfiguration
    {
        public QuoteId QuoteId { get; set; }
    }

    public class QuoteId
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public ChoiceType ext_DataCenter { get; set; }
        public string ext_RefAgreement { get; set; }
    }

    public class QuoteDetailLineItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool? ext_taxexempt { get; set; }
    }

    //Response Class for Line items calculated financial fields
    public class LineItemResponse
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal ext_ListPriceWithAdjustments { get; set; }
        public decimal ext_AdjustmentAmount { get; set; }
        public decimal ext_AdjustmentDiscountPercent { get; set; }
        public decimal ext_USDAdjustment { get; set; }
        public decimal ext_AnnualContractValue { get; set; }
        public decimal ext_AnnualListPrice { get; set; }
        public decimal ext_EstimatedTax { get; set; }
        public decimal ext_USDBasePrice { get; set; }
        public decimal ext_USDTotalValue { get; set; }
        public decimal ext_USDAnnualListPrice { get; set; }
        public decimal ext_USDListPrice { get; set; }
        public decimal ext_USDSalesPrice { get; set; }
        public decimal ext_USDAnnualContractValue { get; set; }
        public decimal ext_EstimatedTotal { get; set; }
        public decimal ext_SalesPrice { get; set; }
        //public decimal? NetPrice { get; set; }
    }
    #endregion

    //removed the incorrect namespace declaration. This might have got added as part of the merge conflicts
    public class LineItem
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "quoteID")]
        public string QuoteId { get; set; }

        [JsonProperty(PropertyName = "productID")]
        public string ProductId { get; set; }

        [JsonProperty(PropertyName = "dailyExchangeRate")]
        public decimal? FXRate { get; set; }

        [JsonProperty(PropertyName = "dailyExchangeRateInUSD")]
        public decimal? FXRateDailyUSD { get; set; }

        [JsonProperty(PropertyName = "listPrice")]
        public decimal? ListPrice { get; set; }

        [JsonProperty(PropertyName = "number")]
        public int? LineNumber { get; set; }

        [JsonProperty(PropertyName = "itemSequenceNumber")]
        public string ItemSequenceNumber { get; set; }

        [JsonProperty(PropertyName = "parentBundleID")]
        public int? ParentBundleNumber { get; set; }

        [JsonProperty(PropertyName = "productHours")]
        public int? ProductHours { get; set; }

        [JsonProperty(PropertyName = "doNotCalculateNetNewACV")]
        public bool? DoNotCalculateNNACV { get; set; }

        [JsonProperty(PropertyName = "variablePriceACVInUSD")]
        public decimal? USDVariablePriceACVCalc { get; set; }

        [JsonProperty(PropertyName = "salesPrice")]
        public decimal? SalesPrice { get; set; }

        [JsonProperty(PropertyName = "netPrice")]
        public decimal? NetPrice { get; set; }

        [JsonProperty(PropertyName = "variablePriceACVCalculate")]
        public decimal? VariablePriceACVCalc { get; set; }

        [JsonProperty(PropertyName = "baseAmount")]
        public decimal? PriceAdjustment { get; set; }

        [JsonProperty(PropertyName = "renewalLineACV")]
        public decimal? RenewalLineACV { get; set; }

        [JsonProperty(PropertyName = "startDate")]
        public DateTime? StartDate { get; set; }

        [JsonProperty(PropertyName = "endDate")]
        public DateTime? EndDate { get; set; }

        [JsonProperty(PropertyName = "term")]
        public int? Term { get; set; }

        [JsonProperty(PropertyName = "summaryLineACVInUSD")]
        public decimal? USDSummaryLineACV { get; set; }

        [JsonProperty(PropertyName = "netNewACVInUSD")]
        public decimal? USDNetNewACV { get; set; }

        [JsonProperty(PropertyName = "quantity")]
        public int? Quantity { get; set; }

        [JsonProperty(PropertyName = "netNewACV")]
        public decimal? NetNewACV { get; set; }

        [JsonProperty(PropertyName = "totalValue")]
        public decimal? ExtendedPrice { get; set; }

        [JsonProperty(PropertyName = "listPriceInUSD")]
        public decimal? USDListPrice { get; set; }

        [JsonProperty(PropertyName = "basePriceInUSD")]
        public decimal? USDBasePrice { get; set; }
        public decimal? pricingExchangeRate { get; set; }

        [JsonProperty(PropertyName = "totalValueInUSD")]
        public decimal? USDTotalValue { get; set; }

        [JsonProperty(PropertyName = "salesPriceInUSD")]
        public decimal? USDSalesPrice { get; set; }

        [JsonProperty(PropertyName = "annualListPriceInUSD")]
        public decimal? AnnualListPrice { get; set; }
        public decimal? annualListPrice { get; set; }

        [JsonProperty(PropertyName = "annualContractAmountInUSD")]
        public decimal? USDAnnualContractValue { get; set; }

        [JsonProperty(PropertyName = "annualContractValue")]
        public decimal? AnnualContractValue { get; set; }

        [JsonProperty(PropertyName = "deployment")]
        public string Deployment { get; set; }

        [JsonProperty(PropertyName = "resourcePlan")]
        public string ResourcePlan { get; set; }

        [JsonProperty(PropertyName = "taskID")]
        public string TaskID { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string LineStatus { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string LineType { get; set; }

        [JsonProperty(PropertyName = "ext_InstanceName")]
        public string InstanceName { get; set; }

        public Product Product { get; set; }

        [JsonProperty(PropertyName = "ext_SoldLINameInstMap")]
        public List<string> SoldLINameInstMap { get; set; }

        [JsonProperty(PropertyName = "ext_InstalledLINameInstMap")]
        public List<string> InstalledLINameInstMap { get; set; }

        [JsonProperty(PropertyName = "ext_SoldLINumInstMap")]
        public List<string> SoldLINumInstMap { get; set; }

        [JsonProperty(PropertyName = "ext_InstalledLINumInstMap")]
        public List<string> InstalledLINumInstMap { get; set; }

        [JsonProperty(PropertyName = "ext_LINumInstMap")]
        public int LINumInstMap { get; set; }        

    }
}
