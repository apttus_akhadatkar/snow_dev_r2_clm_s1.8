﻿/****************************************************************************************
@Name: RequestConfigModel.cs
@Author: Mahesh Patel
@CreateDate: 1 Sep 2017
@Description: Request Config related properties 
@UsedBy: This will be used as a entity for Request Config

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

namespace Apttus.SNowPS.Model
{
    public class RequestConfigModel
    {
        public string[] select { get; set; }

        public string externalFilterField { get; set; }

        public string apttusFilterField { get; set; }

        public string apttusFieldToBeAssigned { get; set; }

        public string objectName { get; set; }

        public string accessToken { get; set; }

        public string resolvedID { get; set; }

        public SearchType searchType { get; set; }

        public string appUrl { get; set; }

        public string UpsertKey { get; set; }
    }

    public enum SearchType
    {
        Normal,
        AQL,
        CpqAdminAPI
    }
}