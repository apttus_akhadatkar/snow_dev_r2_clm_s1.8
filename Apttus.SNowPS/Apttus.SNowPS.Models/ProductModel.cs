﻿
/****************************************************************************************
@Name: ProductModel.cs
@Author: Mahesh Patel
@CreateDate: 25 Sep 2017
@Description: Product related properties 
@UsedBy: This will be used as a entity for Product, Attributes, Plugins

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Apttus.SNowPS.Model
{
    public class Product
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public SelectOption ConfigurationType { get; set; }

        public SelectOption Family { get; set; }

        public string Description { get; set; }

        [JsonProperty(PropertyName = "ext_SKU")]
        public string SKU { get; set; }

        public string ProductCode { get; set; }

        public string ExpirationDate { get; set; }

        public string EffectiveDate { get; set; }

        public SelectOption Uom { get; set; }

        [JsonProperty(PropertyName = "ext_AppVersion")]
        public string AppVersion { get; set; }

        [JsonProperty(PropertyName = "ext_BusinessUnit")]
        public SelectOption BusinessUnit { get; set; }

        [JsonProperty(PropertyName = "ext_CalculatedACV")]
        public bool? CalculateACV { get; set; }

        [JsonProperty(PropertyName = "ext_ContributesTo")]
        public SelectOption ContributesTo { get; set; }

        [JsonProperty(PropertyName = "ext_DefaultRevRec")]
        public SelectOption DefaultRevRec { get; set; }

        [JsonProperty(PropertyName = "ext_ExcludeFromOpportunityFinancials")]
        public bool? ExcludeFromOpportunityFinancials { get; set; }

        [JsonProperty(PropertyName = "ext_ExcludeFromQuoteFinancials")]
        public bool? ExcludeFromQuoteFinancials { get; set; }

        [JsonProperty(PropertyName = "ext_Family")]
        public SelectOption ExtFamily { get; set; }

        [JsonProperty(PropertyName = "ext_Family1")]
        public SelectOption Family1 { get; set; }

        [JsonProperty(PropertyName = "ext_Family2")]
        public SelectOption Family2 { get; set; }

        [JsonProperty(PropertyName = "ext_Family3")]
        public SelectOption Family3 { get; set; }

        [JsonProperty(PropertyName = "ext_FilteredOrdersTypes")]
        public string[] FilteredOrderTypes { get; set; }

        [JsonProperty(PropertyName = "ext_HasQuantityRestrictions")]
        public bool? HasQuantityRestrictions { get; set; }

        [JsonProperty(PropertyName = "ext_LicenseProductType")]
        public SelectOption LicenseProductType { get; set; }

        [JsonProperty(PropertyName = "ext_MaximumQuantity")]
        public string MaximumQuantity { get; set; }

        [JsonProperty(PropertyName = "ext_MinimumQuantity")]
        public string MinimumQuantity { get; set; }

        [JsonProperty(PropertyName = "ext_ProductApprovalStatus")]
        public SelectOption ProductApprovalStatus { get; set; }

        [JsonProperty(PropertyName = "ext_ProductsFirstReleaseVersion")]
        public SelectOption ProductFirstReleaseVersion { get; set; }

        [JsonProperty(PropertyName = "ext_LastReleasesVersion")]
        public SelectOption ProductLastReleaseVersion { get; set; }

        [JsonProperty(PropertyName = "ext_ProductOptionApplicationPercentageAllocation")]
        public string ProductOptionApplicationPercentageAllocation { get; set; }

        [JsonProperty(PropertyName = "ext_ProductLifecycleStatus")]
        public SelectOption ProductLifecycleStatus { get; set; }

        [JsonProperty(PropertyName = "ext_RevRecGroup")]
        public SelectOption RevRecGroup { get; set; }

        [JsonProperty(PropertyName = "ext_SysId")]
        public string SysId { get; set; }

        [JsonProperty(PropertyName = "ext_TaxRateType")]
        public SelectOption TaxRateType { get; set; }

        [JsonProperty(PropertyName = "ext_TechnologyPartnerEmail")]
        public string TechnologyPartnerEmail { get; set; }

        [JsonProperty(PropertyName = "ext_TechnologyPartnerName")]
        public string TechnologyPartnerName { get; set; }

        [JsonProperty(PropertyName = "ext_UAAttribute")]
        public SelectOption UAAttribute { get; set; }

        [JsonProperty(PropertyName = "ext_OrderFormOnly")]
        public SelectOption OrderFormOnly { get; set; }

        [JsonProperty(PropertyName = "ext_NeedDeploymentApplication")]
        public bool? NeedDeploymentApplication { get; set; }

        [JsonProperty(PropertyName = "ext_EnableHIRSA")]
        public bool? EnableHIRSA { get; set; }

        [JsonProperty(PropertyName = "ext_RequiresSurvey")]
        public bool? RequiresSurvey { get; set; }

        [JsonProperty(PropertyName = "ext_DeploymentCategory")]
        public SelectOption DeploymentCategory { get; set; }

        [JsonProperty(PropertyName = "ext_RequiresGoLive")]
        public bool? RequiresGoLive { get; set; }

        [JsonProperty(PropertyName = "ext_Practice")]
        public SelectOption Practice { get; set; }

        [JsonProperty(PropertyName = "ext_RoleGeneric")]
        public SelectOption RoleGroup { get; set; }

        [JsonProperty(PropertyName = "ext_RequiresStatusReport")]
        public bool? RequiresStatusReport { get; set; }

        public Options Options { get; set; }
        public Plugins Plugins { get; set; }
        public Attributes Attributes { get; set; }
    }

    public class SelectOption
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class Options
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public Parentproductid ParentProductId { get; set; }
    }

    public class Parentproductid
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class Plugins
    {
        [JsonProperty(PropertyName = "id")]
        public string ext_PluginId { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string ext_PluginName { get; set; }

        [JsonProperty(PropertyName = "firstReleaseVersion")]
        public string ext_PluginfirstReleaseVersion { get; set; }

        [JsonProperty(PropertyName = "lastReleaseVersion")]
        public string ext_PluginlastReleaseVersion { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string ext_PluginDescription { get; set; }

        [JsonProperty(PropertyName = "isActive")]
        public bool? ext_AutomaticallyOnOff { get; set; }
    }

    public class Attributes
    {
        public string Field { get; set; }
        public string Name { get; set; }
        public List<string> Values { get; set; }
    }

    public class ProductAttributes
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "value")]
        public List<string> Values { get; set; }
    }

    public class Application
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "parentID")]
        public string ParentID { get; set; }

        [JsonProperty(PropertyName = "optionName")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "skuNumber")]
        public string SKUNumber { get; set; }

        [JsonProperty(PropertyName = "configurationType")]
        public string ConfigurationType { get; set; }

        [JsonProperty(PropertyName = "productCode")]
        public string ProductCode { get; set; }

        [JsonProperty(PropertyName = "businessUnit")]
        public string BusinessUnit { get; set; }

        [JsonProperty(PropertyName = "family1")]
        public string Family1 { get; set; }

        [JsonProperty(PropertyName = "family2")]
        public string Family2 { get; set; }

        [JsonProperty(PropertyName = "family3")]
        public string Family3 { get; set; }

        [JsonProperty(PropertyName = "firstReleaseVersion")]
        public string FirstReleaseVersion { get; set; }

        [JsonProperty(PropertyName = "lastReleaseVersion")]
        public string LastReleaseVersion { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "plugins")]
        public List<Plugins> Plugins { get; set; }
    }

    public class ProductsHierarchy
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "code")]
        public string ProductCode { get; set; }

        [JsonProperty(PropertyName = "configurationType")]
        public string ConfigurationType { get; set; }

		[JsonProperty(PropertyName = "family")]
		public string Family { get; set; }

		[JsonProperty(PropertyName = "endDate")]
        public string EndDate { get; set; }

        [JsonProperty(PropertyName = "startDate")]
        public string StartDate { get; set; }

        [JsonProperty(PropertyName = "unitOfMeasure")]
        public string Uom { get; set; }

        [JsonProperty(PropertyName = "appVersion")]
        public string AppVersion { get; set; }

        [JsonProperty(PropertyName = "businessUnit")]
        public string BusinessUnit { get; set; }

        [JsonProperty(PropertyName = "calculateACV")]
        public bool? CalculateACV { get; set; }

        [JsonProperty(PropertyName = "contributesTo")]
        public string ContributesTo { get; set; }

        [JsonProperty(PropertyName = "defaultREVRec")]
        public string DefaultRevRec { get; set; }

        [JsonProperty(PropertyName = "excludeOpportunityFinancials")]
        public bool? ExcludeFromOpportunityFinancials { get; set; }

        [JsonProperty(PropertyName = "excludeQuoteFinancials")]
        public bool? ExcludeFromQuoteFinancials { get; set; }

        [JsonProperty(PropertyName = "category")]
        public string ExtFamily { get; set; }

        [JsonProperty(PropertyName = "family1")]
        public string Family1 { get; set; }

        [JsonProperty(PropertyName = "family2")]
        public string Family2 { get; set; }

        [JsonProperty(PropertyName = "family3")]
        public string Family3 { get; set; }

        [JsonProperty(PropertyName = "orderTypes")]
        public string FilteredOrderTypes { get; set; }

        [JsonProperty(PropertyName = "quantityRestrictions")]
        public bool? HasQuantityRestrictions { get; set; }

        [JsonProperty(PropertyName = "licenseType")]
        public string LicenseProductType { get; set; }

        [JsonProperty(PropertyName = "maxQuantity")]
        public string MaximumQuantity { get; set; }

        [JsonProperty(PropertyName = "minQuantity")]
        public string MinimumQuantity { get; set; }

        [JsonProperty(PropertyName = "approvalStatus")]
        public string ProductApprovalStatus { get; set; }

        [JsonProperty(PropertyName = "firstReleaseVersion")]
        public string ProductFirstReleaseVersion { get; set; }

        [JsonProperty(PropertyName = "lastReleaseVersion")]
        public string ProductLastReleaseVersion { get; set; }

        [JsonProperty(PropertyName = "optionAllocationPercentage")]
        public string ProductOptionApplicationPercentageAllocation { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string ProductLifecycleStatus { get; set; }

        [JsonProperty(PropertyName = "recGroup")]
        public string RevRecGroup { get; set; }

        [JsonProperty(PropertyName = "skuNumber")]
        public string SKU { get; set; }

        [JsonProperty(PropertyName = "surfID")]
        public string SysId { get; set; }

        [JsonProperty(PropertyName = "taxRateType")]
        public string TaxRateType { get; set; }

        [JsonProperty(PropertyName = "technologyPartnerEmailID")]
        public string TechnologyPartnerEmail { get; set; }

        [JsonProperty(PropertyName = "technologyPartnerName")]
        public string TechnologyPartnerName { get; set; }

        [JsonProperty(PropertyName = "uaAttribute")]
        public string UAAttribute { get; set; }

        [JsonProperty(PropertyName = "orderFormSection")]
        public string OrderFormSection { get; set; }

        [JsonProperty(PropertyName = "requiresDeploymentApplication")]
        public bool? NeedDeploymentApplication { get; set; }

        [JsonProperty(PropertyName = "enableHIRSA")]
        public bool? EnableHIRSA { get; set; }

        [JsonProperty(PropertyName = "requiresSurvey")]
        public bool? RequiresSurvey { get; set; }

        [JsonProperty(PropertyName = "deploymentCategory")]
        public string DeploymentCategory { get; set; }

        [JsonProperty(PropertyName = "requiresGoLive")]
        public bool? RequiresGoLive { get; set; }

        [JsonProperty(PropertyName = "practice")]
        public string Practice { get; set; }

        [JsonProperty(PropertyName = "roleGroup")]
        public string RoleGroup { get; set; }

        [JsonProperty(PropertyName = "requiresStatusReport")]
        public bool? RequiresStatusReport { get; set; }

        [JsonIgnore]
        public List<Attributes> Attributes { get; set; }

        [JsonProperty(PropertyName = "attributes")]
        public List<ProductAttributes> ProductAttributes { get; set; }

        [JsonProperty(PropertyName = "applications")]
        public List<Application> Applications { get; set; }
    }
}
