﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apttus.SNowPS.Model
{
    public class ResponseModel
    {
        public dynamic[] SerializedResultEntities { get; set; }

        public int TotalNumberOfRecords { get; set; }

        public bool MoreRecords { get; set; }
    }
}