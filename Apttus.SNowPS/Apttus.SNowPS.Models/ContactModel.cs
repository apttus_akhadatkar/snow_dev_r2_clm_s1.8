﻿/****************************************************************************************
@Name: ContactModel.cs
@Author: Varun Shah
@CreateDate: 25 Sep 2017
@Description:Contact related properties 
@UsedBy: This will be used as a entity for Contact

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

using Newtonsoft.Json;

namespace Apttus.SNowPS.Model
{
    public class ContactModel
    {
        [JsonProperty(PropertyName = "Id")]
        public string id { get; set; }

        [JsonProperty(PropertyName = "ext_ParentCustomerMDMID")]
        public string mdmID { get; set; }

        [JsonProperty(PropertyName = "ext_Active")]
        public bool? active { get; set; }

        [JsonProperty(PropertyName = "BusinessPhone")]
        public string businessPhone { get; set; }

        [JsonProperty(PropertyName = "ext_DoNotEmail")]
        public bool? doNotEmail { get; set; }

        [JsonProperty(PropertyName = "ext_DoNotPhone")]
        public bool? doNotPhone { get; set; }

        [JsonProperty(PropertyName = "EmailAddress1")]
        public string emailAddress { get; set; }

        [JsonProperty(PropertyName = "FirstName")]
        public string firstName { get; set; }

        [JsonProperty(PropertyName = "ext_JobTitle")]
        public string jobTitle { get; set; }

        [JsonProperty(PropertyName = "LastName")]
        public string lastName { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string name { get; set; }

        [JsonProperty(PropertyName = "MobilePhone")]
        public string mobilePhone { get; set; }

        [JsonProperty(PropertyName = "ext_Salutation")]
        public string salutation { get; set; }

        [JsonProperty(PropertyName = "ExternalId")]
        public string consumerID { get; set; }
    }
}
