﻿/****************************************************************************************
@Name: AccesToken.cs
@Author: Mahesh Patel
@CreateDate: 1 Sep 2017
@Description: Access Token Model 
@UsedBy: This will be used as a entity for Access Token

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using System.Runtime.Serialization;

namespace Apttus.SNowPS.Model
{
    [DataContract]
    public class AccessTokenModel
    {
        [DataMember(Name = "token_type")]
        public string TokenType { get; set; }

        [DataMember(Name = "expires_in")]
        public string ExpiresIn { get; set; }

        [DataMember(Name = "ext_expires_in")]
        public string ExtExpiresIn { get; set; }

        [DataMember(Name = "expires_on")]
        public string ExpiresOn { get; set; }

        [DataMember(Name = "not_before")]
        public string NotBefore { get; set; }

        [DataMember(Name = "resource")]
        public string Resource { get; set; }

        [DataMember(Name = "access_token")]
        public string Token { get; set; }
    }
}
