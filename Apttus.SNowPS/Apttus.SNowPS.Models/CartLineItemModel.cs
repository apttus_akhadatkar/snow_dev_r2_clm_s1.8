﻿/*************************************************************
@Name: CartLineItemModel.cs
@Author: Chirag Modi    
@CreateDate: 11-Oct-2017
@Description: This class contains classes & properties for 'Cart Line Item' and related objects.
@UsedBy: Description of how this class is being used
******************************************************************
@ModifiedBy: Author who modified this class
@ModifiedDate: Date the class was modified
@ChangeDescription: A brief description of what was modified
 
 
**** PS: @ModifiedBy and @ChangeDescription does not apply to ‘In Development’ code – should be used for major changes in functionality or subsequent releases.
******************************************************************/
using Newtonsoft.Json;
using System;

namespace Apttus.SNowPS.Model
{
    public class CartLineItemModel
    {
        public String Id { get; set; }
        public decimal? Quantity { get; set; }
        //public String ext_UpsellQuantity { get; set; }
        public double? NetUnitPrice { get; set; }
        public double? NetPrice { get; set; }
        public decimal? SellingTerm { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double? ext_NNACV { get; set; }
        public double? ext_TCV { get; set; }
        public double? ext_RenewalACV { get; set; }
        public double? ext_BlendedPrice { get; set; }
        //public List<KeyValue> ext_FlaggedAsList { get; set; }
        public string ext_FlaggedAs1 { get; set; }
        public decimal? ext_DeltaQuantity { get; set; }
        public int LineNumber { get; set; }
        public int ItemSequence { get; set; }
        public decimal? AssetQuantity { get; set; }
        public LookUp AssetLineItemId { get; set; }
        public AssetLineItem ALI { get; set; }

        //Changes by asha 
        public double ext_ListPriceWithAdjustments { get; set; }
        public double? ExtendedPrice { get; set; }
        public double? ext_AdjustmentAmount { get; set; }
        public double? ListPrice { get; set; }
        public double? ext_AdjustmentDiscountPercent { get; set; }
        public int Term { get; set; }
        public double? ext_AnnualContractValue { get; set; }
        public double? ext_AnnualListPrice { get; set; }
        public double? ext_EstimatedTotal { get; set; }
        public ChoiceType SellingFrequency { get; set; }
        public double? BasePrice { get; set; }
        public double? ext_EstimatedTax { get; set; }
        public double? ext_SalesPrice { get; set; }
        public Product ProductId { get; set; }
        public Configuration ConfigurationId { get; set; }
        public Quote Q { get; set; }
        public Agreement AGR { get; set; }

        public class Configuration
        {
            public string Id { get; set; }
            public QuoteId QuoteId { get; set; }
        }

        public class Quote
        {
            public ChoiceType ext_Type { get; set; }
        }

        public class Product
        {
            public string Name { get; set; }
            public string Id { get; set; }
            public ChoiceType ext_Family { get; set; }
        }

        public class AssetLineItem
        {
            public string Id { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public decimal? Quantity { get; set; }
            public double? NetUnitPrice { get; set; }
            public double? ext_BlendedPrice { get; set; }
        }

        public class Agreement
        {
            public QuoteId ext_quoteid;
        }

        public class QuoteId
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public ChoiceType ext_DataCenter { get; set; }
            public string ext_RefAgreement { get; set; }
        }


    }


}
