﻿/*************************************************************
@Name: ParticipantRoleModel.cs
@Author: Bhavinkumar Mistry    
@CreateDate: 01-Nov-2017
@Description: This class contains classes & properties for 'ParticipantRole' and related objects.
@UsedBy: Description of how this class is being used
******************************************************************
@ModifiedBy: Author who modified this class
@ModifiedDate: Date the class was modified
@ChangeDescription: A brief description of what was modified
 
 
**** PS: @ModifiedBy and @ChangeDescription does not apply to ‘In Development’ code – should be used for major changes in functionality or subsequent releases.
******************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apttus.SNowPS.Model
{
    public class ParticipantRoleModel
    {
        public string Name { get; set; }
        public LookUp ParticipantId { get; set; }
        public SelectOption Role { get; set; }
    }
}
