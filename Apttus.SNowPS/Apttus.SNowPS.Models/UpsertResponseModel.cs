﻿/****************************************************************************************
@Name: UpsertResponseModel.cs
@Author: Akash Kakadiya
@CreateDate: 18 Oct 2017
@Description:  
@UsedBy: This will be used by mule response

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
namespace Apttus.SNowPS.Model
{
    public class UpsertResponseModel
    {
        public string Id { get; set; }
        public string ExternalId { get; set; }
    }
}
