﻿/****************************************************************************************
@Name: AssetLineItemController.cs
@Author: Kiran Satani
@CreateDate: 13 Oct 2017
@Description: for AssetLineItem Synch  
@UsedBy: This will be used by Mule as an API call

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Apttus.SNowPS.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace Apttus.SNowPS.API.Controllers
{
	[ServiceRequestActionFilter]
	public class AssetLineItemController : ApiController
	{
		AssetLineItemRepository _assetLineItem = new AssetLineItemRepository();

		[ActionName("Sync")]
		[HttpPost]
		public HttpResponseMessage SyncToMule(Dictionary<string, object> dicAssetLineItem)
		{
			try
			{
                List<Dictionary<string, object>> lstDicAssetLineItem = new List<Dictionary<string, object>>();
                lstDicAssetLineItem.Add(dicAssetLineItem);
                HttpResponseMessage httpMuleResponseMessage = null;
				if (lstDicAssetLineItem != null && lstDicAssetLineItem.Count > 0)
				{
					//Generate authentication token
					var accessToken = Utilities.GetAuthToken();

					var dicAssetLineItemIngnoreCase = Utilities.GetCaseIgnoreDictContent(lstDicAssetLineItem);
                    string Id = string.Empty;
                    if (dicAssetLineItemIngnoreCase.FirstOrDefault().ContainsKey(Constants.FIELD_ID))
                        Id = Convert.ToString(dicAssetLineItemIngnoreCase.Select(x => x[Constants.FIELD_ID]).FirstOrDefault());
                    if (string.IsNullOrEmpty(Id))
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Please Pass Asset LineItem Id");
                    

					//Get Product Json For Mule
					var assetLineItemJsonForMule = _assetLineItem.GetAssetLineItemDetail(Id, accessToken);

					//Call Mule API
					if (!string.IsNullOrEmpty(assetLineItemJsonForMule))
					{
						MuleHeaderModel objMuleHeaderModel = new MuleHeaderModel();
						objMuleHeaderModel.APIName = ConfigurationManager.AppSettings[Constants.CONFIG_ASSETSYNCAPI]; //Constants.CONFIG_QUOTESYNCAPI
						objMuleHeaderModel.MuleUrl = ConfigurationManager.AppSettings[Constants.CONFIG_MOCKAPIURL];
						objMuleHeaderModel.XClientId = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXCLIENTID];
                        objMuleHeaderModel.HttpContentStr = new StringContent(assetLineItemJsonForMule, Encoding.UTF8, "application/json");
						objMuleHeaderModel.AccessToken = Utilities.GetMuleAuthToken();
						httpMuleResponseMessage = HttpActions.PostRequestMule(objMuleHeaderModel);
						if (httpMuleResponseMessage.IsSuccessStatusCode)
						{
							//var muleResponseDict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(httpMuleResponseMessage.Content.ReadAsStringAsync().Result);
							//var ignoreCaseMuleResDict = Utilities.Utilities.GetCaseIgnoreSingleDictContent(muleResponseDict);
							//var id = ignoreCaseMuleResDict[Constants.FIELD_ID];
							//var externalId = ignoreCaseMuleResDict[Constants.FIELD_CONSUMERID];
							//var response = Utilities.Utilities.UpdateMuleResponse(Convert.ToString(id), Convert.ToString(externalId), Constants.OBJ_CLM_AGREEMENT);
							//return Utilities.CreateResponse(httpMuleResponseMessage);
						}
						return httpMuleResponseMessage;
					}
					else
						return Request.CreateResponse(HttpStatusCode.OK, "Data Not Found In Apttus System");
				}
				else
					return Request.CreateResponse(HttpStatusCode.BadRequest, "Please Pass Asset LineItem Id");
			}
			catch (Exception ex)
			{
				return Request.CreateResponse(HttpStatusCode.BadRequest, "Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace);
			}
		}
	}
}
