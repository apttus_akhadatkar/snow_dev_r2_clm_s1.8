﻿
/****************************************************************************************
@Name: OrderController.cs
@Author: Meera Kant
@CreateDate: 27 Sep 2017
@Description: Syncs Order, Order Line Items and Invoice Schedules to SAP by calling the Mule API 
@UsedBy: This will be used by Apttus when Order is finalized/activated

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Apttus.SNowPS.Respository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace Apttus.SNowPS.API.Controllers
{
    [ServiceRequestActionFilter]
    public class OrderController : ApiController
    {
        private readonly OrderRepository _order = new OrderRepository();
        /// <summary>
        /// Order Advance Search
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Get()
        {
            try
            {
                string jsonResponse = string.Empty;
                if (this.Request != null && this.Request.RequestUri != null && !string.IsNullOrEmpty(this.Request.RequestUri.Query))
                {
                    var headers = this.Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;

                    var encodedQuery = this.Request.RequestUri.Query;
                    var decodedQuery = Utilities.GetDecodedQuery(encodedQuery);

                    var reqConfig = new RequestConfigModel();
                    reqConfig.accessToken = accessToken;
                    reqConfig.objectName = Constants.OBJ_ORDER;

                    var response = Utilities.GetSearch(decodedQuery, reqConfig);

                    return Utilities.CreateResponse(response);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }

        /// <summary>
        /// Get Order,Order Line & Invoice Schedule by OrderId to Send Mule Soft API
        /// </summary>
        /// <param name="dicOrder"></param>
        /// <returns></returns>
        [ActionName("Sync")]
        [HttpPost]
        public HttpResponseMessage SyncToMule(Dictionary<string, object> dicContent)
        {
            try
            {
                HttpResponseMessage httpMuleResponseMessage = null;
                if (dicContent != null && dicContent.Count > 0)
                {
                    //Generate authentication token
                    var accessToken = Utilities.GetAuthToken();

					//Get case-insensitive 
					var dicOrder = Utilities.GetCaseIgnoreSingleDictContent(dicContent);

                    string Id = dicOrder.ContainsKey(Constants.FIELD_ID) && dicOrder[Constants.FIELD_ID] != null ? Convert.ToString(dicOrder[Constants.FIELD_ID]) : null;

					//Get Product Json For Mule
					var orderJsonForMule = _order.GetOrderDetail(Id, accessToken);

                    //Call Mule API
                    if (!string.IsNullOrEmpty(orderJsonForMule))
                    {
                        MuleHeaderModel objMuleHeaderModel = new MuleHeaderModel();
                        objMuleHeaderModel.APIName = ConfigurationManager.AppSettings[Constants.CONFIG_ORDERSYNCAPI];
                        objMuleHeaderModel.MuleUrl = ConfigurationManager.AppSettings[Constants.CONFIG_MOCKAPIURL];
                        objMuleHeaderModel.XClientId = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXCLIENTID];
                        objMuleHeaderModel.HttpContentStr = new StringContent(orderJsonForMule, Encoding.UTF8, "application/json");
                        objMuleHeaderModel.AccessToken = Utilities.GetMuleAuthToken();
                        httpMuleResponseMessage = HttpActions.PostRequestMule(objMuleHeaderModel);
                        //if (httpMuleResponseMessage.IsSuccessStatusCode)
                        //{
                        //    var muleResponseDict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(httpMuleResponseMessage.Content.ReadAsStringAsync().Result);
                        //    var ignoreCaseMuleResDict = Utilities.GetCaseIgnoreSingleDictContent(muleResponseDict);
                        //    var id = ignoreCaseMuleResDict[Constants.FIELD_ID];
                        //    var externalId = ignoreCaseMuleResDict[Constants.FIELD_CONSUMERID];
                        //    var response = Utilities.UpdateMuleResponse(Convert.ToString(id), Convert.ToString(externalId), Constants.OBJ_ORDER);
                        //    return Utilities.CreateResponse(response);
                        //}
                        return httpMuleResponseMessage;
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, "Data Not Found In Apttus System");
                }
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Please Pass Order Id");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }

        #region PS
        /// <summary>
        /// On 'Activate Order' button click, update Activation date, call 'SAP Integration API' with Order#, update response data back to Order/Order lines.
        /// </summary>
        /// <param name="dicOrder"></param>
        /// <returns></returns>
        [ActionName("ActivateOrder")]
        [HttpPost]
        public HttpResponseMessage OnActivateOrder(List<Dictionary<string, object>> dicOrder)
        {
            try
            {
                if (dicOrder != null && dicOrder.Count > 0)
                {
                    //Generate authentication token
                    var headers = Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : Utilities.GetAuthToken();
                    var dicOrderIngnoreCase = Utilities.GetCaseIgnoreDictContent(dicOrder);
                    string orderId = Convert.ToString(dicOrderIngnoreCase.Select(x => x[Constants.FIELD_ID]).FirstOrDefault());

                    //Get Order details by OrderId
                    var orderData = _order.GetOrderByOrderId(orderId, accessToken);

                    //Get Order Line Item details by OrderId
                    var orderLineItemData = _order.GetOrderLineItemsByOrderId(orderId, accessToken);

                    //Update 'Order.ReadyForActivationDate' = current System Date.
                    orderData.ActivatedDate = DateTime.Now;

                    //Call 'SAP integration API' with Order# to push the Order and Order Lines to SAP - ToDo


                    //Update ext_SAPContractLineID, ext_SAPContractID and ext_IntegrationError(if any) from the SAP response
                    //On ‘Order’ to store 'ext_IntegrationError', ‘if in response from HI or SAP you get an error message, concatenate that to the existing message in the Long field..
                    //  ... and prefix it with the source system (HI or SAP) with the time stamp’ 
                    var successResponse = true;
                    if (successResponse)
                    {
                        orderData.ext_SAPContractID = "SAP Contract Id"; //ToDo: Update SAP Contract Id.
                        orderLineItemData.First().ext_SAPContractLineID = "SAP Contract Line Id"; //ToDo: Update SAP Contract Line Id for each line item.
                    }
                    else
                    {
                        orderData.ext_IntegrationError += "---Source System: 'SAP', Time Stamp: '" + DateTime.Now.ConvertToString()
                                                                + "', Error Message: '" + "Message from SAP'" + "'---"; //ToDo: Update SAP error message.
                    }

                    //Save Order and Order Line Items..
                    var orderDetails = new List<dynamic>();
                    orderDetails.Add(new
                    {
                        Id = orderData.Id,
                        ActivatedDate = orderData.ActivatedDate,
                        ext_SAPContractID = orderData.ext_SAPContractID,
                        //ext_IntegrationError = orderData.ext_IntegrationError //ToDo: Uncomment when custom field 'ext_IntegrationError' is created.
                    });
                    var orderUpdateResponse = _order.UpdateOrderData(orderDetails, accessToken);
                    //var orderUpdateResponse = _order.UpdateOrderData(new List<Order>() { orderData }, accessToken);

                    var orderLineItemDetails = new List<dynamic>();
                    foreach (var oli in orderLineItemData)
                    {
                        orderLineItemDetails.Add(new
                        {
                            Id = oli.Id,
                            ext_SAPContractLineID = oli.ext_SAPContractLineID
                        });
                    }
                    var orderLineItemsUpdateResponse = _order.UpdateOrderLineItemsData(orderLineItemDetails, accessToken);

                    //If 'SAP Order Integration' completes successfully, Grey out(disable) 'Activate Order' button. 


                    return orderLineItemsUpdateResponse;
                }
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Please Pass Order Id");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace);
            }
        }

        /// <summary>
        /// Scheduled API: For Scheduled Provisioning, Filter all the unprocessed Line Items and send them to HI using PushToHI API.
        /// </summary>
        /// <param name="dicOrder"></param>
        /// <returns></returns>
        [ActionName("ScheduledProvisioningForHI")]
        [HttpPost]
        public HttpResponseMessage ScheduledProvisioningForHI(List<Dictionary<string, object>> dicOrder)
        {
            try
            {
                if (dicOrder != null && dicOrder.Count > 0)
                {
                    //Generate authentication token
                    var headers = Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : Utilities.GetAuthToken();
                    var dicOrderIngnoreCase = Utilities.GetCaseIgnoreDictContent(dicOrder);
                    string orderId = Convert.ToString(dicOrderIngnoreCase.Select(x => x[Constants.FIELD_ID]).FirstOrDefault());

                    //Get Order Line Item details by OrderId
                    var orderLineItemData = _order.GetOrderLineItemsByOrderId(orderId, accessToken);

                    //Get all the Filtered Order Line Items where HI Integration ‘HI processed flag’ = ‘False’ and Scheduled Provisioning date <= Current System date
                    var filteredOrderLineItemData = orderLineItemData.Where(oli => (!oli.ext_HiIntegrationProcessed.HasValue || !oli.ext_HiIntegrationProcessed.Value)
                                                        && oli.ext_ScheduledProvisionDate.HasValue && oli.ext_ScheduledProvisionDate <= DateTime.Now).ToList();

                    //Push all the filtered Line Items to HI
                    if (filteredOrderLineItemData.Count > 0)
                        return PushAllFilteredLineItemsToHI(filteredOrderLineItemData, orderId, accessToken);
                    else
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "No Filtered Line Items found.");
                }
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Please Pass Order Id");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace);
            }
        }

        /// <summary>
        /// On-Demand API: On 'PushToHI' button click, update Activation date, call 'SAP Integration API' with Order#, update response data back to Order/Order lines.
        /// </summary>
        /// <param name="dicOrder"></param>
        /// <returns></returns>
        [ActionName("PushToHI")]
        [HttpPost]
        public HttpResponseMessage PushToHI(List<Dictionary<string, object>> dicOrder)
        {
            try
            {
                if (dicOrder != null && dicOrder.Count > 0)
                {
                    //Generate authentication token
                    var headers = Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : Utilities.GetAuthToken();
                    var dicOrderIngnoreCase = Utilities.GetCaseIgnoreDictContent(dicOrder);
                    string orderId = Convert.ToString(dicOrderIngnoreCase.Select(x => x[Constants.FIELD_ID]).FirstOrDefault());

                    //Get Order Line Item details by OrderId
                    var orderLineItemData = _order.GetOrderLineItemsByOrderId(orderId, accessToken);

                    //Get all the Filtered Order Line Items where HI Integration ‘HI processed flag’ = ‘False’ and Scheduled Provisioning date <= Current System date
                    var filteredOrderLineItemData = orderLineItemData.Where(oli => (!oli.ext_HiIntegrationProcessed.HasValue || !oli.ext_HiIntegrationProcessed.Value)
                                                        && oli.ext_ScheduledProvisionDate.HasValue && oli.ext_ScheduledProvisionDate <= DateTime.Now).ToList();

                    //For all Line Items, If the Schedule Provisioning date is in the past, override the provisioning date with ‘Current System Date’ on order line item
                    foreach (var oli in filteredOrderLineItemData)
                    {
                        if (oli.ext_ScheduledProvisionDate < DateTime.Now)
                        {
                            oli.ext_ScheduledProvisionDate = DateTime.Now;
                        }
                    }

                    //Push all the filtered Line Items to HI
                    if (filteredOrderLineItemData.Count > 0)
                        return PushAllFilteredLineItemsToHI(filteredOrderLineItemData, orderId, accessToken);
                    else
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "No Filtered Line Items found.");
                }
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Please Pass Order Id");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace);
            }
        }

        /// <summary>
        /// Private method: To push filtered Line Items to HI
        /// </summary>
        /// <param name="orderLineItems"></param>
        /// <param name="orderId"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        private HttpResponseMessage PushAllFilteredLineItemsToHI(List<OLI> orderLineItems, string orderId, string accessToken)
        {
            try
            {
                HttpResponseMessage httpResponseMessage = null;

                //Invoke the integration api for the HI integration with all filterd Line Items. //ToDo


                //On ‘Order’ to store 'ext_IntegrationError', ‘if in response from HI or SAP you get an error message, concatenate that to the existing message in the Long field..
                //  ... and prefix it with the source system (HI or SAP) with the time stamp’ 
                var successResponse = true;
                if (successResponse)
                {
                    //Set 'HI Integration processed' flag = true which should Grey out(disable)'Push To HI' button.
                    //foreach (var oli in orderLineItems)
                    //{
                    //    oli.ext_HiIntegrationProcessed = true;
                    //}

                    //Save Order Line Items
                    var orderLineItemDetails = new List<dynamic>();
                    foreach (var oli in orderLineItems)
                    {
                        orderLineItemDetails.Add(new
                        {
                            Id = oli.Id,
                            ext_HiIntegrationProcessed = true
                        });
                    }
                    httpResponseMessage = _order.UpdateOrderLineItemsData(orderLineItemDetails, accessToken);
                }
                else
                {
                    //Get Order details by OrderId
                    var orderData = _order.GetOrderByOrderId(orderId, accessToken);

                    orderData.ext_IntegrationError += "---Source System: 'HI', Time Stamp: '" + DateTime.Now.ConvertToString()
                                                            + "', Error Message: '" + "Message from HI'" + "'---"; //ToDo: Update HI error message.

                    //Save Order data
                    var orderDetails = new List<dynamic>();
                    orderDetails.Add(new
                    {
                        Id = orderData.Id,
                        //ext_IntegrationError = orderData.ext_IntegrationError  //ToDo: Uncomment when custom field 'ext_IntegrationError' is created.
                    });
                    httpResponseMessage = _order.UpdateOrderData(orderDetails, accessToken);
                }

                return httpResponseMessage;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace);
            }
        }

        #endregion PS
        
        
		[ActionName("SyncToHi")]
		public HttpResponseMessage SyncToHi([FromBody]Dictionary<string,object> dictContent)
		{
			try
			{
				HttpResponseMessage httpMuleResponseMessage = null;
				if (dictContent != null && dictContent.Count > 0)
				{
					//Generate authentication token
					var accessToken = Utilities.GetAuthToken();

					//Get case-insensitive 
					var dicOrder = Utilities.GetCaseIgnoreSingleDictContent(dictContent);

					string Id = dicOrder.ContainsKey(Constants.FIELD_ID) && dicOrder[Constants.FIELD_ID] != null ? Convert.ToString(dicOrder[Constants.FIELD_ID]) : null;

					//Get Product Json For Mule
					var agreementJsonForMule = _order.GetOrderDetailforHi(Id, accessToken);

					//Call Mule API
					if (!string.IsNullOrEmpty(agreementJsonForMule))
					{
						MuleHeaderModel objMuleHeaderModel = new MuleHeaderModel();
						objMuleHeaderModel.APIName = ConfigurationManager.AppSettings[Constants.CONFIG_ORDERSYNCIAPI];
						objMuleHeaderModel.MuleUrl = ConfigurationManager.AppSettings[Constants.CONFIG_MOCKAPIURL];
						objMuleHeaderModel.XClientId = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXCLIENTID];
						objMuleHeaderModel.HttpContentStr = new StringContent(agreementJsonForMule, Encoding.UTF8, "application/json");
						objMuleHeaderModel.AccessToken = Utilities.GetMuleAuthToken();
						httpMuleResponseMessage = HttpActions.PostRequestMule(objMuleHeaderModel);
						if (httpMuleResponseMessage.IsSuccessStatusCode)
						{
							var muleResponseDict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(httpMuleResponseMessage.Content.ReadAsStringAsync().Result);
							var ignoreCaseMuleResDict = Utilities.GetCaseIgnoreSingleDictContent(muleResponseDict);
							if (ignoreCaseMuleResDict.ContainsKey(Constants.FIELD_ID) && ignoreCaseMuleResDict.ContainsKey(Constants.FIELD_CONSUMERID))
							{
								var id = ignoreCaseMuleResDict[Constants.FIELD_ID];
								var externalId = ignoreCaseMuleResDict[Constants.FIELD_CONSUMERID];
								var response = Utilities.UpdateMuleResponse(Convert.ToString(id), Convert.ToString(externalId), Constants.OBJ_AGREEMENT);
								return Utilities.CreateResponse(response);
							}
							else
								return httpMuleResponseMessage;
						}
						return httpMuleResponseMessage;
					}
					else
						return Request.CreateResponse(HttpStatusCode.OK, "Data Not Found In Apttus System");
				}
				else
					return Request.CreateResponse(HttpStatusCode.BadRequest, "Please Pass Agreement Id");
			}
			catch (Exception ex)
			{
				return Request.CreateResponse(HttpStatusCode.BadRequest, "Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace);
			}
		}
        
	}
}
