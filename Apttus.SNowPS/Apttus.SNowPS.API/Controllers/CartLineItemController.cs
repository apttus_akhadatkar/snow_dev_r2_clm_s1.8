﻿/*************************************************************
@Name: CartLineItemController.cs
@Author: Chirag Modi
@CreateDate: 11-Oct-2017
@Description: This class contains pre and post methods for Cart Line Items.
@UsedBy: Description of how this class is being used
******************************************************************
@ModifiedBy: Author who modified this class
@ModifiedDate: Date the class was modified
@ChangeDescription: A brief description of what was modified
 
 
**** PS: @ModifiedBy and @ChangeDescription does not apply to ‘In Development’ code – should be used for major changes in functionality or subsequent releases.
******************************************************************/
using Apttus.SNowPS.Repository.Renewals;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Apttus.SNowPS.Model;
using Apttus.SNowPS.Common;
using System.Linq;

namespace Apttus.SNowPS.API.Controllers
{
    [ServiceRequestActionFilter]
    public class CartLineItemController : ApiController
    {
        private readonly CartLineItemRepository _cartLineItem = new CartLineItemRepository();
        /// <summary>
        /// Cart Line Item Advance Search
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Get()
        {
            try
            {
                //TODO: Verify & update below code in future if and when needed.
                //string jsonResponse = string.Empty;
                //if (this.Request != null && this.Request.RequestUri != null && !string.IsNullOrEmpty(this.Request.RequestUri.Query))
                //{
                //    var headers = this.Request.Headers;
                //    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;

                //    var encodedQuery = this.Request.RequestUri.Query;
                //    var decodedQuery = Utilities.GetDecodedQuery(encodedQuery);

                //    var reqConfig = new RequestConfigModel();
                //    reqConfig.accessToken = accessToken;
                //    reqConfig.objectName = Constants.OBJ_QUOTE;

                //    var response = Utilities.GetSearch(decodedQuery, reqConfig);

                //    return Utilities.CreateResponse(response);
                //}
                //return Request.CreateResponse(HttpStatusCode.BadRequest, "");
                return null;
            }
            catch (Exception)
            {
                //TODO
                throw;
            }
        }

        /// <summary>
        /// Updates Cart Line Item Financial Fields using standard API
        /// </summary>
        /// <param name="dicContent"></param>
        /// <returns></returns>
        [ActionName("UpdateCartLineItems")]
        [HttpPost]
        public HttpResponseMessage Post(Dictionary<string, object> dicContent)
        {
            try
            {
                if (dicContent == null || dicContent.Count == 0)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Please Provide ConfigurationId");
                }

                var dictLineItems = new List<Dictionary<string, object>> { dicContent } ;
                var accessToken = Request.Headers.Authorization != null ? Convert.ToString(Request.Headers.Authorization) : Utilities.GetAuthToken(); //Generate authentication token
                var dicLineItemIngnoreCase = Utilities.GetCaseIgnoreDictContent(dictLineItems);
                //string id = Convert.ToString(dicLineItemIngnoreCase.Select(x => x[Constants.FIELD_ID]).FirstOrDefault());
                string configurationId = Convert.ToString(dicLineItemIngnoreCase.Select(x => x[Constants.FIELD_CONFIGURATIONID]).FirstOrDefault());

                var cartLineItems = new List<CartLineItemModel>();

                //Get Cart Line Items Details..
                cartLineItems = _cartLineItem.GetCartLineItems(accessToken, configurationId);

                //Arrange Cart Line Items Order By Line Number & Item Sequence..
                cartLineItems = cartLineItems.OrderBy(c => c.LineNumber).ThenBy(c => c.ItemSequence).ToList();

                //Calculate & Update Financial Fields on Cart Line Items..
                cartLineItems = _cartLineItem.CalculateFinancialFields(cartLineItems);

                var response = _cartLineItem.UpdateCartLineItems(accessToken, cartLineItems);

                return response;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace);
            }
        }


        #region PS
        /// <summary>
        /// Calculation of Travel & Expense price 
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        [ActionName("CalculateTravelExpensePrice")]
        [HttpPost]
        public HttpResponseMessage CalculateTravelExpensePrice([FromBody] List<Dictionary<string, object>> dictContent)
        {
            try
            {
                if (dictContent != null && dictContent.Count() > 0)
                {
                    var headers = this.Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;
                    var dicIngnoreCase = Utilities.GetCaseIgnoreDictContent(dictContent);
                    string Id = Convert.ToString(dicIngnoreCase.Select(x => x[Constants.FIELD_ID]).FirstOrDefault());

                    var result = _cartLineItem.CalculateTravelExpensePrice(Id, accessToken);
                    return Request.CreateResponse(HttpStatusCode.OK, Convert.ToString(result.Content.ReadAsStringAsync().Result));
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Quote Id is required");
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace);
            }
        }
        #endregion PS
    }
}
