﻿/****************************************************************************************
@Name: CustomSyncController.cs
@Author: Varun Shah
@CreateDate: 26 Sep 2017
@Description: Generic sync (Apttus to mule/sap) API 
@UsedBy: This will be used by Mule as an API call

@ModifiedBy: Akash Kakadiya
@ModifiedDate: 27 Sep 2017
@ChangeDescription: Add Account Sync Method
*****************************************************************************************/
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace Apttus.SNowPS.API.Controllers
{
    public partial class CustomSyncController : ApiController
    {
        static string appInstance = Convert.ToString(ConfigurationManager.AppSettings[Constants.CONFIG_APPINSTANCE]);
        private string muleXClientId = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXCLIENTID];
        private string muleXSourceSystem = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXSOURCESYSTEM];

        
        /// <summary>
        /// Use for Account Sync 
        /// </summary>
        /// <param name="dictContent"></param>
        /// <param name="sendType"></param>
        /// <returns></returns>
        [ActionName("crm_account")]
        [HttpPost]
        public HttpResponseMessage AccountSync([FromBody] List<Dictionary<string, object>> dictContent)
        {
            try
            {
                string objResponse = string.Empty;
                string resAccountString = string.Empty; 
                string accountUrl = ConfigurationManager.AppSettings[Constants.CONFIG_MOCKAPIURL];
                string accountAPIName = ConfigurationManager.AppSettings[Constants.CONFIG_ACCOUNTSYNCAPINAME];

                dictContent = Utilities.GetCaseIgnoreDictContent(dictContent);
                var requestConfig = Utilities.GetRequestConfiguration(Constants.OBJ_ACCOUNT, Constants.FIELD_ID, Constants.FIELD_ID);

                var resMessageAccount = Utilities.Search(dictContent, requestConfig);
                if (resMessageAccount.IsSuccessStatusCode)
                {
                    resAccountString = resMessageAccount.Content.ReadAsStringAsync().Result;
                    var responseAccount = JObject.Parse(resAccountString).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToObject<List<AccountModel>>().FirstOrDefault();
                    if (responseAccount != null)
                    {
                        responseAccount.accountId = responseAccount.srcPartyId;
                        responseAccount.accountName = responseAccount.partyNm;
                        responseAccount.boClassCode = "Organization";
                        responseAccount.eventType = string.IsNullOrEmpty(responseAccount.eventType) ? "" : responseAccount.eventType; ;

                        var accountPayLoad = new MuleRequestModel() { totalRecords = 1, batchID = "", recordsList = responseAccount.ToString().ToArray() };
                        objResponse = new JavaScriptSerializer().Serialize(accountPayLoad);
                      
                        var accountResponse = HttpActions.PostRequestMule(Utilities.GetMuleHeaderDetail(muleXClientId, accountUrl, objResponse, muleXSourceSystem, accountAPIName));
                        if (accountResponse.IsSuccessStatusCode)
                            return Utilities.CreateResponse(accountResponse);
                        else
                            return Request.CreateResponse(HttpStatusCode.BadRequest, "");
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "");
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "");
            }
            catch (Exception)
            {
                //TODO
                throw;
            }
        }

        /// <summary>
        /// Use for Address Sync
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        [ActionName("cpq_AccountLocation")]
        [HttpPost]
        public HttpResponseMessage AddressSync([FromBody] Dictionary<string, string> dictContent)
        {
            try
            {
                string objResponse = string.Empty;
                string resContactString = string.Empty;
               
                string addressSyncUrl = ConfigurationManager.AppSettings[Constants.CONFIG_MOCKAPIURL];

                List<Dictionary<string, object>> content = new List<Dictionary<string, object>>();
                content.Add(dictContent.ToDictionary(k => k.Key, k => (object)k.Value));
                content = Utilities.GetCaseIgnoreDictContent(content);

                var requestConfig = Utilities.GetRequestConfiguration(Constants.OBJ_ACCOUNTLOCATION, "Id", "Id");

                var resMessageAddress = Utilities.Search(content, requestConfig);

                if (resMessageAddress.IsSuccessStatusCode)
                {
                    resContactString = resMessageAddress.Content.ReadAsStringAsync().Result;
                    var add = JObject.Parse(resContactString).SelectToken("SerializedResultEntities").ToObject<List<Dictionary<string, object>>>().First();

                    var dictAddresses = new Dictionary<string, string>();
                    dictAddresses = add.ToDictionary(k => k.Key, k => Convert.ToString(k.Value));
                    dictAddresses["AccountId"] = dictAddresses["AccountId"] != null ? JsonConvert.DeserializeObject<AccountModel>(dictAddresses["AccountId"]).srcPartyId : null;
                    string accountId = dictAddresses["AccountId"];
                    string str = new JavaScriptSerializer().Serialize(dictAddresses);
                    var responseAddress = JsonConvert.DeserializeObject<AddressModel>(str);

                    if (!string.IsNullOrEmpty(accountId))
                    {
                        var reConfig = Utilities.GetRequestConfiguration(Constants.OBJ_ACCOUNT, "Id", "Id");

                        List<Dictionary<string, object>> contentAccount = new List<Dictionary<string, object>>();
                        var dic = new Dictionary<string, object>();
                        dic.Add("Id", (object)accountId);
                        contentAccount.Add(dic);
                        contentAccount = Utilities.GetCaseIgnoreDictContent(contentAccount);

                        var resMessageAccountAddress = Utilities.Search(contentAccount, reConfig);
                        string resAccountAddressString = string.Empty;

                        if (resMessageAccountAddress.IsSuccessStatusCode)
                        {
                            resAccountAddressString = resMessageAccountAddress.Content.ReadAsStringAsync().Result;
                            var resultAccount = JObject.Parse(resAccountAddressString);
                            var acc = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(resultAccount["SerializedResultEntities"].ToString());
                            var dictAccount = acc.FirstOrDefault();
                            var dictAccounts = new Dictionary<string, string>();
                            if (dictAccount.Count > 0)
                            {
                                dictAccounts = dictAccount.ToDictionary(k => k.Key, k => Convert.ToString(k.Value));
                                dictAccounts["Type"] = dictAccounts["Type"] != null ? JsonConvert.DeserializeObject<KeyValuePair<string, string>>(dictAccounts["Type"]).Value : null;
                                string strAccountAddress = new JavaScriptSerializer().Serialize(dictAccounts);

                                var responseAccountAddress = JsonConvert.DeserializeObject<AddressAccountModel>(strAccountAddress);
                                responseAddress.addressTypeCode = "PRMRY_ADDR";
                                responseAccountAddress.addresses = new List<AddressModel>() { responseAddress };
                                responseAccountAddress.accountId = responseAccountAddress.srcPartyId;
                                //responseAccountAddress.accountName = responseAccountAddress.partyNm;
                                responseAccountAddress.boClassCode = "Organization";
                                responseAccountAddress.eventType = "ADDR_SYNC";

                                objResponse = new JavaScriptSerializer().Serialize(responseAccountAddress);                                
                                var add_AccResponse = HttpActions.PutRequestMule(Utilities.GetMuleHeaderDetail(muleXClientId, addressSyncUrl, objResponse, muleXSourceSystem, "v2/accounts" + "/" + responseAccountAddress.accountId));

                                if (add_AccResponse.IsSuccessStatusCode)
                                    return Utilities.CreateResponse(add_AccResponse);
                                else
                                    return Request.CreateResponse(HttpStatusCode.BadRequest, "");
                            }
                        }
                    }

                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "");
            }
            catch
            {
                //TODO
                throw;
            }
        }

        /// <summary>
        /// Use for Address validation
        /// </summary>
        /// <param name="requestContent"></param>
        /// <returns></returns>
        [HttpPost]
        public string AddressValidator([FromBody]JObject requestContent)
        {
            try
            {
                string addressValidatorApiName  = ConfigurationManager.AppSettings[Constants.CONFIG_ADDRESSVALIDATORAPINAME];
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                AddressValidationModel responseAddr = JsonConvert.DeserializeObject<AddressValidationModel>(requestContent.ToString());              
                
                string muleAddressSearchUrl = ConfigurationManager.AppSettings[Constants.CONFIG_MULEADDRESSSEARCHURL];
                string jsonString = new JavaScriptSerializer().Serialize(responseAddr);
                var responseMessage = HttpActions.PostRequestMule(Utilities.GetMuleHeaderDetail(muleXClientId, muleAddressSearchUrl, jsonString, muleXSourceSystem, addressValidatorApiName));

                if (responseMessage.IsSuccessStatusCode)
                    return responseMessage.Content.ReadAsStringAsync().Result;
                else
                    return responseMessage.ReasonPhrase;
            }
            catch (Exception ex)
            {
                //TODO
                return ex.Message;
            }
        }

        /// <summary>
        /// Use for Address Search
        /// </summary>
        /// <param name="requestContent"></param>
        /// <returns></returns>
        [HttpPost]
        public string AddressSearch([FromBody]JObject requestContent)
        {
            try
            {
                string addressSearchApiName = ConfigurationManager.AppSettings[Constants.CONFIG_ADDRESSSEARCHAPINAME];
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                Dictionary<string, string> queryAddr = JsonConvert.DeserializeObject<Dictionary<string, string>>(requestContent.ToString());

                var muleHaderModel = new MuleHeaderModel();
                muleHaderModel.AccessToken = Utilities.GetMuleAuthToken();
                muleHaderModel.XClientId = muleXClientId;
                muleHaderModel.AppUrl = ConfigurationManager.AppSettings[Constants.CONFIG_APPURL];
                muleHaderModel.MuleUrl = ConfigurationManager.AppSettings[Constants.CONFIG_MOCKAPIURL]; 
                muleHaderModel.APIName = addressSearchApiName; 

                string query = string.Empty;
                if (queryAddr != null)
                {
                    if (queryAddr.Count > 0)
                    {
                        foreach (var parm in queryAddr)
                            query += parm.Key + Constants.CHAR_EQUAL + parm.Value + Constants.CHAR_AMPERSAND;
                    }
                    muleHaderModel.APIName = muleHaderModel.APIName + Constants.CHAR_QUESTIONMARK + query;
                }
                var response = HttpActions.GetRequestMule(muleHaderModel);

                if (response.IsSuccessStatusCode)
                    return response.Content.ReadAsStringAsync().Result;
                else
                    return response.ReasonPhrase;
            }
            catch (Exception)
            {
                //TODO
                throw;
            }
        }

        
    }
}
