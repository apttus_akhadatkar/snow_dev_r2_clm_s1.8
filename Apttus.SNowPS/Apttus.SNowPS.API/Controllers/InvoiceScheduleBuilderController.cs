﻿/****************************************************************************************
@Name: InvoiceScheduleBuilderController.cs
@Author: Maunish P. Shah
@CreateDate: 12 Oct 2017
@Description: Generates the Invoice Schedules 
@UsedBy: This will be used by Mule as an API call

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Apttus.SNowPS.Repository;
using Apttus.SNowPS.Repository.CLM;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace Apttus.SNowPS.API.Controllers
{
    [ServiceRequestActionFilter]
    public class InvoiceScheduleBuilderController : BaseController
    {
        private readonly InvoiceScheduleBuilderRepository _invoiceschedule = new InvoiceScheduleBuilderRepository();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dicQuote"></param>
        /// <returns></returns>
        [ActionName("GenerateInvSchedule")]
        [HttpPost]
        public HttpResponseMessage GenerateInvSchedule(List<Dictionary<string, object>> dicQuote)
        {
            try
            {
                if (dicQuote != null && dicQuote.Count > 0)
                {
                    var headers = this.Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;
                    var dicQuoteIngnoreCase = Utilities.GetCaseIgnoreDictContent(dicQuote);
                    string Id = Convert.ToString(dicQuoteIngnoreCase.Select(x => x[Constants.FIELD_ID]).FirstOrDefault());

                    //Generate authentication token
                    if (accessToken == null)
                        accessToken = Utilities.GetAuthToken();

                    //Get quote header financial fields after calculation
                    var quoteHeaderJson = _invoiceschedule.GenerateInvSchedule(Id, accessToken);
                    if (quoteHeaderJson != null)
                    {
                        //return Utilities.CreateResponse(quoteHeaderJson);
                        return Request.CreateResponse(HttpStatusCode.OK, Convert.ToString(quoteHeaderJson.Content.ReadAsStringAsync().Result));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.NoContent, "No Record(s) Found");
                    }
                }
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Quote Id is required.");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace);
            }
        }

        /// <summary>
        /// Update Invoice schedules based on quote id and agreement id
        /// </summary>
        /// <param name="quoteid">quoteid</param>
        /// <param name="agreementid">agreementid</param>
        /// <returns>Ok status</returns>
        [HttpPut]
        public IHttpActionResult UpdateInvoiceScheduler(Dictionary<string, string> body)
        {
            try
            {
                if (!body.ContainsKey("agreementid") || !body.ContainsKey("agreementid"))
                {
                    throw new Exception("Parameter missing. Please provide proper input.");
                }
                var quoteid = body["quoteid"].ToString();
                var agreementid = body["agreementid"].ToString();
                InvoiceScheduleRepository invoiceScheduleManager = new InvoiceScheduleRepository(AccessToken);
                invoiceScheduleManager.Update(quoteid, agreementid);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
