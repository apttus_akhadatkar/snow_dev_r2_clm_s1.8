﻿/****************************************************************************************
@Name: SNowPSAPI.cs
@Author: Auto Generated
@CreateDate: 1 Sep 2017
@Description: The FabricRuntime creates an instance of this class for each service type instance. 
@UsedBy: 

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using Microsoft.ServiceFabric.Services.Communication.Runtime;
using Microsoft.ServiceFabric.Services.Runtime;
using System.Collections.Generic;
using System.Fabric;
using System.Fabric.Description;
using System.Linq;

namespace Apttus.SNowPS.API
{
    /// <summary>
    /// The FabricRuntime creates an instance of this class for each service type instance. 
    /// </summary>
    internal sealed class SNowPSAPI : StatelessService {
        public SNowPSAPI(StatelessServiceContext context)
            : base(context) { }

        /// <summary>
        /// Optional override to create listeners (like tcp, http) for this service instance.
        /// </summary>
        /// <returns>The collection of listeners.</returns>

        protected override IEnumerable<ServiceInstanceListener> CreateServiceInstanceListeners()
        {
            var endpoints = Context.CodePackageActivationContext.GetEndpoints()
                                   .Where(endpoint => endpoint.Protocol == EndpointProtocol.Http || endpoint.Protocol == EndpointProtocol.Https)
                                   .Select(endpoint => endpoint.Name);

            var startup = new Startup(Context);

            return endpoints.Select(endpoint => new ServiceInstanceListener(
                serviceContext => new OwinCommunicationListener(startup.ConfigureApp, serviceContext, ServiceEventSource.Current, endpoint), endpoint));
        }

    }
}
